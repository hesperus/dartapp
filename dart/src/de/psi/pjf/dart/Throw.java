/**
 */
package de.psi.pjf.dart;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Throw</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.psi.pjf.dart.Throw#getParticipation <em>Participation</em>}</li>
 *   <li>{@link de.psi.pjf.dart.Throw#getFirst <em>First</em>}</li>
 *   <li>{@link de.psi.pjf.dart.Throw#getFirstType <em>First Type</em>}</li>
 *   <li>{@link de.psi.pjf.dart.Throw#getSecond <em>Second</em>}</li>
 *   <li>{@link de.psi.pjf.dart.Throw#getSecondType <em>Second Type</em>}</li>
 *   <li>{@link de.psi.pjf.dart.Throw#getThird <em>Third</em>}</li>
 *   <li>{@link de.psi.pjf.dart.Throw#getThirdType <em>Third Type</em>}</li>
 *   <li>{@link de.psi.pjf.dart.Throw#getTotal <em>Total</em>}</li>
 *   <li>{@link de.psi.pjf.dart.Throw#getDoubles <em>Doubles</em>}</li>
 *   <li>{@link de.psi.pjf.dart.Throw#getTriples <em>Triples</em>}</li>
 *   <li>{@link de.psi.pjf.dart.Throw#isBest <em>Best</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.psi.pjf.dart.DartPackage#getThrow()
 * @model annotation="http://www.eclipse.org/emf/2002/GenModel image='false'"
 * @generated
 */
public interface Throw extends EObject
{
  /**
   * Returns the value of the '<em><b>Participation</b></em>' container reference.
   * It is bidirectional and its opposite is '{@link de.psi.pjf.dart.Participation#getThrows <em>Throws</em>}'.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Participation</em>' container reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Participation</em>' container reference.
   * @see #setParticipation(Participation)
   * @see de.psi.pjf.dart.DartPackage#getThrow_Participation()
   * @see de.psi.pjf.dart.Participation#getThrows
   * @model opposite="throws" transient="false"
   * @generated
   */
  Participation getParticipation();

  /**
   * Sets the value of the '{@link de.psi.pjf.dart.Throw#getParticipation <em>Participation</em>}' container reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Participation</em>' container reference.
   * @see #getParticipation()
   * @generated
   */
  void setParticipation(Participation value);

  /**
   * Returns the value of the '<em><b>First</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>First</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>First</em>' attribute.
   * @see #setFirst(int)
   * @see de.psi.pjf.dart.DartPackage#getThrow_First()
   * @model unique="false"
   * @generated
   */
  int getFirst();

  /**
   * Sets the value of the '{@link de.psi.pjf.dart.Throw#getFirst <em>First</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>First</em>' attribute.
   * @see #getFirst()
   * @generated
   */
  void setFirst(int value);

  /**
   * Returns the value of the '<em><b>First Type</b></em>' attribute.
   * The literals are from the enumeration {@link de.psi.pjf.dart.ThrowType}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>First Type</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>First Type</em>' attribute.
   * @see de.psi.pjf.dart.ThrowType
   * @see #setFirstType(ThrowType)
   * @see de.psi.pjf.dart.DartPackage#getThrow_FirstType()
   * @model unique="false"
   * @generated
   */
  ThrowType getFirstType();

  /**
   * Sets the value of the '{@link de.psi.pjf.dart.Throw#getFirstType <em>First Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>First Type</em>' attribute.
   * @see de.psi.pjf.dart.ThrowType
   * @see #getFirstType()
   * @generated
   */
  void setFirstType(ThrowType value);

  /**
   * Returns the value of the '<em><b>Second</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Second</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Second</em>' attribute.
   * @see #setSecond(int)
   * @see de.psi.pjf.dart.DartPackage#getThrow_Second()
   * @model unique="false"
   * @generated
   */
  int getSecond();

  /**
   * Sets the value of the '{@link de.psi.pjf.dart.Throw#getSecond <em>Second</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Second</em>' attribute.
   * @see #getSecond()
   * @generated
   */
  void setSecond(int value);

  /**
   * Returns the value of the '<em><b>Second Type</b></em>' attribute.
   * The literals are from the enumeration {@link de.psi.pjf.dart.ThrowType}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Second Type</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Second Type</em>' attribute.
   * @see de.psi.pjf.dart.ThrowType
   * @see #setSecondType(ThrowType)
   * @see de.psi.pjf.dart.DartPackage#getThrow_SecondType()
   * @model unique="false"
   * @generated
   */
  ThrowType getSecondType();

  /**
   * Sets the value of the '{@link de.psi.pjf.dart.Throw#getSecondType <em>Second Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Second Type</em>' attribute.
   * @see de.psi.pjf.dart.ThrowType
   * @see #getSecondType()
   * @generated
   */
  void setSecondType(ThrowType value);

  /**
   * Returns the value of the '<em><b>Third</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Third</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Third</em>' attribute.
   * @see #setThird(int)
   * @see de.psi.pjf.dart.DartPackage#getThrow_Third()
   * @model unique="false"
   * @generated
   */
  int getThird();

  /**
   * Sets the value of the '{@link de.psi.pjf.dart.Throw#getThird <em>Third</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Third</em>' attribute.
   * @see #getThird()
   * @generated
   */
  void setThird(int value);

  /**
   * Returns the value of the '<em><b>Third Type</b></em>' attribute.
   * The literals are from the enumeration {@link de.psi.pjf.dart.ThrowType}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Third Type</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Third Type</em>' attribute.
   * @see de.psi.pjf.dart.ThrowType
   * @see #setThirdType(ThrowType)
   * @see de.psi.pjf.dart.DartPackage#getThrow_ThirdType()
   * @model unique="false"
   * @generated
   */
  ThrowType getThirdType();

  /**
   * Sets the value of the '{@link de.psi.pjf.dart.Throw#getThirdType <em>Third Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Third Type</em>' attribute.
   * @see de.psi.pjf.dart.ThrowType
   * @see #getThirdType()
   * @generated
   */
  void setThirdType(ThrowType value);

  /**
   * Returns the value of the '<em><b>Total</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Total</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Total</em>' attribute.
   * @see de.psi.pjf.dart.DartPackage#getThrow_Total()
   * @model unique="false" transient="true" changeable="false" volatile="true" derived="true"
   *        annotation="http://www.eclipse.org/emf/2002/GenModel get='<%de.psi.pjf.dart.Throw%> _this = this;\nint _first = _this.getFirst();\n<%de.psi.pjf.dart.Throw%> _this_1 = this;\n<%de.psi.pjf.dart.ThrowType%> _firstType = _this_1.getFirstType();\nint _value = _firstType.getValue();\nint _multiply = (_first * _value);\n<%de.psi.pjf.dart.Throw%> _this_2 = this;\nint _second = _this_2.getSecond();\n<%de.psi.pjf.dart.Throw%> _this_3 = this;\n<%de.psi.pjf.dart.ThrowType%> _secondType = _this_3.getSecondType();\nint _value_1 = _secondType.getValue();\nint _multiply_1 = (_second * _value_1);\nint _plus = (_multiply + _multiply_1);\n<%de.psi.pjf.dart.Throw%> _this_4 = this;\nint _third = _this_4.getThird();\n<%de.psi.pjf.dart.Throw%> _this_5 = this;\n<%de.psi.pjf.dart.ThrowType%> _thirdType = _this_5.getThirdType();\nint _value_2 = _thirdType.getValue();\nint _multiply_2 = (_third * _value_2);\nint _plus_1 = (_plus + _multiply_2);\nreturn _plus_1;'"
   * @generated
   */
  int getTotal();

  /**
   * Returns the value of the '<em><b>Doubles</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Doubles</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Doubles</em>' attribute.
   * @see de.psi.pjf.dart.DartPackage#getThrow_Doubles()
   * @model unique="false" transient="true" changeable="false" volatile="true" derived="true"
   *        annotation="http://www.eclipse.org/emf/2002/GenModel get='int _xblockexpression = (int) 0;\n{\n\tint _xifexpression = (int) 0;\n\t<%de.psi.pjf.dart.Throw%> _this = this;\n\t<%de.psi.pjf.dart.ThrowType%> _firstType = _this.getFirstType();\n\tboolean _equals = <%com.google.common.base.Objects%>.equal(_firstType, <%de.psi.pjf.dart.ThrowType%>.DOUBLE);\n\tif (_equals)\n\t{\n\t\t_xifexpression = 1;\n\t}\n\telse\n\t{\n\t\t_xifexpression = 0;\n\t}\n\tint doubles = _xifexpression;\n\tint _xifexpression_1 = (int) 0;\n\t<%de.psi.pjf.dart.Throw%> _this_1 = this;\n\t<%de.psi.pjf.dart.ThrowType%> _secondType = _this_1.getSecondType();\n\tboolean _equals_1 = <%com.google.common.base.Objects%>.equal(_secondType, <%de.psi.pjf.dart.ThrowType%>.DOUBLE);\n\tif (_equals_1)\n\t{\n\t\tint _plus = (doubles + 1);\n\t\t_xifexpression_1 = _plus;\n\t}\n\telse\n\t{\n\t\t_xifexpression_1 = doubles;\n\t}\n\tdoubles = _xifexpression_1;\n\tint _xifexpression_2 = (int) 0;\n\t<%de.psi.pjf.dart.Throw%> _this_2 = this;\n\t<%de.psi.pjf.dart.ThrowType%> _thirdType = _this_2.getThirdType();\n\tboolean _equals_2 = <%com.google.common.base.Objects%>.equal(_thirdType, <%de.psi.pjf.dart.ThrowType%>.DOUBLE);\n\tif (_equals_2)\n\t{\n\t\tint _plus_1 = (doubles + 1);\n\t\t_xifexpression_2 = _plus_1;\n\t}\n\telse\n\t{\n\t\t_xifexpression_2 = doubles;\n\t}\n\tdoubles = _xifexpression_2;\n\t_xblockexpression = (doubles);\n}\nreturn _xblockexpression;'"
   * @generated
   */
  int getDoubles();

  /**
   * Returns the value of the '<em><b>Triples</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Triples</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Triples</em>' attribute.
   * @see de.psi.pjf.dart.DartPackage#getThrow_Triples()
   * @model unique="false" transient="true" changeable="false" volatile="true" derived="true"
   *        annotation="http://www.eclipse.org/emf/2002/GenModel get='int _xblockexpression = (int) 0;\n{\n\tint _xifexpression = (int) 0;\n\t<%de.psi.pjf.dart.Throw%> _this = this;\n\t<%de.psi.pjf.dart.ThrowType%> _firstType = _this.getFirstType();\n\tboolean _equals = <%com.google.common.base.Objects%>.equal(_firstType, <%de.psi.pjf.dart.ThrowType%>.TRIPLE);\n\tif (_equals)\n\t{\n\t\t_xifexpression = 1;\n\t}\n\telse\n\t{\n\t\t_xifexpression = 0;\n\t}\n\tint triples = _xifexpression;\n\tint _xifexpression_1 = (int) 0;\n\t<%de.psi.pjf.dart.Throw%> _this_1 = this;\n\t<%de.psi.pjf.dart.ThrowType%> _secondType = _this_1.getSecondType();\n\tboolean _equals_1 = <%com.google.common.base.Objects%>.equal(_secondType, <%de.psi.pjf.dart.ThrowType%>.TRIPLE);\n\tif (_equals_1)\n\t{\n\t\tint _plus = (triples + 1);\n\t\t_xifexpression_1 = _plus;\n\t}\n\telse\n\t{\n\t\t_xifexpression_1 = triples;\n\t}\n\ttriples = _xifexpression_1;\n\tint _xifexpression_2 = (int) 0;\n\t<%de.psi.pjf.dart.Throw%> _this_2 = this;\n\t<%de.psi.pjf.dart.ThrowType%> _thirdType = _this_2.getThirdType();\n\tboolean _equals_2 = <%com.google.common.base.Objects%>.equal(_thirdType, <%de.psi.pjf.dart.ThrowType%>.TRIPLE);\n\tif (_equals_2)\n\t{\n\t\tint _plus_1 = (triples + 1);\n\t\t_xifexpression_2 = _plus_1;\n\t}\n\telse\n\t{\n\t\t_xifexpression_2 = triples;\n\t}\n\ttriples = _xifexpression_2;\n\t_xblockexpression = (triples);\n}\nreturn _xblockexpression;'"
   * @generated
   */
  int getTriples();

  /**
   * Returns the value of the '<em><b>Best</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Best</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Best</em>' attribute.
   * @see de.psi.pjf.dart.DartPackage#getThrow_Best()
   * @model unique="false" transient="true" changeable="false" volatile="true" derived="true"
   *        annotation="http://www.eclipse.org/emf/2002/GenModel get='boolean _xblockexpression = false;\n{\n\t<%de.psi.pjf.dart.Throw%> _this = this;\n\t<%de.psi.pjf.dart.Participation%> _participation = _this.getParticipation();\n\t<%de.psi.pjf.dart.Player%> _player = null;\n\tif (_participation!=null)\n\t{\n\t\t_player=_participation.getPlayer();\n\t}\n\tfinal <%de.psi.pjf.dart.Player%> player = _player;\n\tboolean _xifexpression = false;\n\tboolean _equals = <%com.google.common.base.Objects%>.equal(player, null);\n\tif (_equals)\n\t{\n\t\t_xifexpression = false;\n\t}\n\telse\n\t{\n\t\t<%de.psi.pjf.dart.Throw%> _this_1 = this;\n\t\tint _total = _this_1.getTotal();\n\t\tint _bestThrow = player.getBestThrow();\n\t\tboolean _equals_1 = (_total == _bestThrow);\n\t\t_xifexpression = _equals_1;\n\t}\n\t_xblockexpression = (_xifexpression);\n}\nreturn _xblockexpression;'"
   * @generated
   */
  boolean isBest();

} // Throw
