/**
 */
package de.psi.pjf.dart;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Player</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.psi.pjf.dart.Player#getName <em>Name</em>}</li>
 *   <li>{@link de.psi.pjf.dart.Player#getLeague <em>League</em>}</li>
 *   <li>{@link de.psi.pjf.dart.Player#getParticipations <em>Participations</em>}</li>
 *   <li>{@link de.psi.pjf.dart.Player#getWinCount <em>Win Count</em>}</li>
 *   <li>{@link de.psi.pjf.dart.Player#getBestThrow <em>Best Throw</em>}</li>
 *   <li>{@link de.psi.pjf.dart.Player#getGameCount <em>Game Count</em>}</li>
 *   <li>{@link de.psi.pjf.dart.Player#getThrowsAvg <em>Throws Avg</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.psi.pjf.dart.DartPackage#getPlayer()
 * @model annotation="http://www.eclipse.org/emf/2002/GenModel image='false'"
 * @generated
 */
public interface Player extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see de.psi.pjf.dart.DartPackage#getPlayer_Name()
   * @model unique="false" id="true"
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link de.psi.pjf.dart.Player#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>League</b></em>' container reference.
   * It is bidirectional and its opposite is '{@link de.psi.pjf.dart.League#getPlayers <em>Players</em>}'.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>League</em>' container reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>League</em>' container reference.
   * @see #setLeague(League)
   * @see de.psi.pjf.dart.DartPackage#getPlayer_League()
   * @see de.psi.pjf.dart.League#getPlayers
   * @model opposite="players" transient="false"
   * @generated
   */
  League getLeague();

  /**
   * Sets the value of the '{@link de.psi.pjf.dart.Player#getLeague <em>League</em>}' container reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>League</em>' container reference.
   * @see #getLeague()
   * @generated
   */
  void setLeague(League value);

  /**
   * Returns the value of the '<em><b>Participations</b></em>' reference list.
   * The list contents are of type {@link de.psi.pjf.dart.Participation}.
   * It is bidirectional and its opposite is '{@link de.psi.pjf.dart.Participation#getPlayer <em>Player</em>}'.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Participations</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Participations</em>' reference list.
   * @see de.psi.pjf.dart.DartPackage#getPlayer_Participations()
   * @see de.psi.pjf.dart.Participation#getPlayer
   * @model opposite="player"
   * @generated
   */
  EList<Participation> getParticipations();

  /**
   * Returns the value of the '<em><b>Win Count</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Win Count</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Win Count</em>' attribute.
   * @see de.psi.pjf.dart.DartPackage#getPlayer_WinCount()
   * @model unique="false" transient="true" changeable="false" volatile="true" derived="true"
   *        annotation="http://www.eclipse.org/emf/2002/GenModel get='<%de.psi.pjf.dart.Player%> _this = this;\n<%org.eclipse.emf.common.util.EList%><<%de.psi.pjf.dart.Participation%>> _participations = _this.getParticipations();\nfinal <%org.eclipse.xtext.xbase.lib.Functions.Function1%><<%de.psi.pjf.dart.Participation%>,<%java.lang.Boolean%>> _function = new <%org.eclipse.xtext.xbase.lib.Functions.Function1%><<%de.psi.pjf.dart.Participation%>,<%java.lang.Boolean%>>()\n{\n\tpublic <%java.lang.Boolean%> apply(final <%de.psi.pjf.dart.Participation%> it)\n\t{\n\t\tboolean _isWinner = it.isWinner();\n\t\treturn <%java.lang.Boolean%>.valueOf(_isWinner);\n\t}\n};\n<%java.lang.Iterable%><<%de.psi.pjf.dart.Participation%>> _filter = <%org.eclipse.xtext.xbase.lib.IterableExtensions%>.<<%de.psi.pjf.dart.Participation%>>filter(_participations, _function);\nint _size = <%org.eclipse.xtext.xbase.lib.IterableExtensions%>.size(_filter);\nreturn _size;'"
   * @generated
   */
  int getWinCount();

  /**
   * Returns the value of the '<em><b>Best Throw</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Best Throw</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Best Throw</em>' attribute.
   * @see de.psi.pjf.dart.DartPackage#getPlayer_BestThrow()
   * @model unique="false" transient="true" changeable="false" volatile="true" derived="true"
   *        annotation="http://www.eclipse.org/emf/2002/GenModel get='<%de.psi.pjf.dart.Player%> _this = this;\n<%org.eclipse.emf.common.util.EList%><<%de.psi.pjf.dart.Participation%>> _participations = _this.getParticipations();\nfinal <%org.eclipse.xtext.xbase.lib.Functions.Function2%><<%java.lang.Integer%>,<%de.psi.pjf.dart.Participation%>,<%java.lang.Integer%>> _function = new <%org.eclipse.xtext.xbase.lib.Functions.Function2%><<%java.lang.Integer%>,<%de.psi.pjf.dart.Participation%>,<%java.lang.Integer%>>()\n{\n\tpublic <%java.lang.Integer%> apply(final <%java.lang.Integer%> a, final <%de.psi.pjf.dart.Participation%> b)\n\t{\n\t\tint _gameMax = b.getGameMax();\n\t\tint _max = <%java.lang.Math%>.max((a).intValue(), _gameMax);\n\t\treturn <%java.lang.Integer%>.valueOf(_max);\n\t}\n};\n<%java.lang.Integer%> _fold = <%org.eclipse.xtext.xbase.lib.IterableExtensions%>.<<%de.psi.pjf.dart.Participation%>, <%java.lang.Integer%>>fold(_participations, <%java.lang.Integer%>.valueOf(0), _function);\nreturn (_fold).intValue();'"
   * @generated
   */
  int getBestThrow();

  /**
   * Returns the value of the '<em><b>Game Count</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Game Count</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Game Count</em>' attribute.
   * @see de.psi.pjf.dart.DartPackage#getPlayer_GameCount()
   * @model unique="false" transient="true" changeable="false" volatile="true" derived="true"
   *        annotation="http://www.eclipse.org/emf/2002/GenModel get='<%de.psi.pjf.dart.Player%> _this = this;\n<%org.eclipse.emf.common.util.EList%><<%de.psi.pjf.dart.Participation%>> _participations = _this.getParticipations();\nint _size = _participations.size();\nreturn _size;'"
   * @generated
   */
  int getGameCount();

  /**
   * Returns the value of the '<em><b>Throws Avg</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Throws Avg</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Throws Avg</em>' attribute.
   * @see de.psi.pjf.dart.DartPackage#getPlayer_ThrowsAvg()
   * @model unique="false" transient="true" changeable="false" volatile="true" derived="true"
   *        annotation="http://www.eclipse.org/emf/2002/GenModel get='double _xblockexpression = (double) 0;\n{\n\t<%de.psi.pjf.dart.Player%> _this = this;\n\t<%org.eclipse.emf.common.util.EList%><<%de.psi.pjf.dart.Participation%>> _participations = _this.getParticipations();\n\tfinal <%org.eclipse.xtext.xbase.lib.Functions.Function2%><<%java.lang.Integer%>,<%de.psi.pjf.dart.Participation%>,<%java.lang.Integer%>> _function = new <%org.eclipse.xtext.xbase.lib.Functions.Function2%><<%java.lang.Integer%>,<%de.psi.pjf.dart.Participation%>,<%java.lang.Integer%>>()\n\t{\n\t\tpublic <%java.lang.Integer%> apply(final <%java.lang.Integer%> a, final <%de.psi.pjf.dart.Participation%> b)\n\t\t{\n\t\t\t<%org.eclipse.emf.common.util.EList%><<%de.psi.pjf.dart.Throw%>> _throws = b.getThrows();\n\t\t\tfinal <%org.eclipse.xtext.xbase.lib.Functions.Function2%><<%java.lang.Integer%>,<%de.psi.pjf.dart.Throw%>,<%java.lang.Integer%>> _function = new <%org.eclipse.xtext.xbase.lib.Functions.Function2%><<%java.lang.Integer%>,<%de.psi.pjf.dart.Throw%>,<%java.lang.Integer%>>()\n\t\t\t{\n\t\t\t\tpublic <%java.lang.Integer%> apply(final <%java.lang.Integer%> c, final <%de.psi.pjf.dart.Throw%> d)\n\t\t\t\t{\n\t\t\t\t\tint _total = d.getTotal();\n\t\t\t\t\tint _plus = (_total + (c).intValue());\n\t\t\t\t\treturn <%java.lang.Integer%>.valueOf(_plus);\n\t\t\t\t}\n\t\t\t};\n\t\t\t<%java.lang.Integer%> _fold = <%org.eclipse.xtext.xbase.lib.IterableExtensions%>.<<%de.psi.pjf.dart.Throw%>, <%java.lang.Integer%>>fold(_throws, <%java.lang.Integer%>.valueOf(0), _function);\n\t\t\tint _plus = ((a).intValue() + (_fold).intValue());\n\t\t\treturn <%java.lang.Integer%>.valueOf(_plus);\n\t\t}\n\t};\n\tfinal <%java.lang.Integer%> up = <%org.eclipse.xtext.xbase.lib.IterableExtensions%>.<<%de.psi.pjf.dart.Participation%>, <%java.lang.Integer%>>fold(_participations, <%java.lang.Integer%>.valueOf(0), _function);\n\t<%de.psi.pjf.dart.Player%> _this_1 = this;\n\t<%org.eclipse.emf.common.util.EList%><<%de.psi.pjf.dart.Participation%>> _participations_1 = _this_1.getParticipations();\n\tfinal <%org.eclipse.xtext.xbase.lib.Functions.Function2%><<%java.lang.Integer%>,<%de.psi.pjf.dart.Participation%>,<%java.lang.Integer%>> _function_1 = new <%org.eclipse.xtext.xbase.lib.Functions.Function2%><<%java.lang.Integer%>,<%de.psi.pjf.dart.Participation%>,<%java.lang.Integer%>>()\n\t{\n\t\tpublic <%java.lang.Integer%> apply(final <%java.lang.Integer%> a, final <%de.psi.pjf.dart.Participation%> b)\n\t\t{\n\t\t\t<%org.eclipse.emf.common.util.EList%><<%de.psi.pjf.dart.Throw%>> _throws = b.getThrows();\n\t\t\tint _size = _throws.size();\n\t\t\tint _plus = ((a).intValue() + _size);\n\t\t\treturn <%java.lang.Integer%>.valueOf(_plus);\n\t\t}\n\t};\n\t<%java.lang.Integer%> down = <%org.eclipse.xtext.xbase.lib.IterableExtensions%>.<<%de.psi.pjf.dart.Participation%>, <%java.lang.Integer%>>fold(_participations_1, <%java.lang.Integer%>.valueOf(0), _function_1);\n\tdouble _xifexpression = (double) 0;\n\tboolean _equals = ((down).intValue() == 0.0);\n\tif (_equals)\n\t{\n\t\t_xifexpression = 0.0;\n\t}\n\telse\n\t{\n\t\tint _divide = ((up).intValue() / (down).intValue());\n\t\t_xifexpression = _divide;\n\t}\n\t_xblockexpression = (_xifexpression);\n}\nreturn _xblockexpression;'"
   * @generated
   */
  double getThrowsAvg();

} // Player
