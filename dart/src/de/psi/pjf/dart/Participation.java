/**
 */
package de.psi.pjf.dart;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Participation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.psi.pjf.dart.Participation#getGame <em>Game</em>}</li>
 *   <li>{@link de.psi.pjf.dart.Participation#getThrows <em>Throws</em>}</li>
 *   <li>{@link de.psi.pjf.dart.Participation#getPlayer <em>Player</em>}</li>
 *   <li>{@link de.psi.pjf.dart.Participation#getToWin <em>To Win</em>}</li>
 *   <li>{@link de.psi.pjf.dart.Participation#getGameMax <em>Game Max</em>}</li>
 *   <li>{@link de.psi.pjf.dart.Participation#isWinner <em>Winner</em>}</li>
 *   <li>{@link de.psi.pjf.dart.Participation#getGameAvg <em>Game Avg</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.psi.pjf.dart.DartPackage#getParticipation()
 * @model annotation="http://www.eclipse.org/emf/2002/GenModel image='false'"
 * @generated
 */
public interface Participation extends EObject
{
  /**
   * Returns the value of the '<em><b>Game</b></em>' container reference.
   * It is bidirectional and its opposite is '{@link de.psi.pjf.dart.Game#getParticipations <em>Participations</em>}'.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Game</em>' container reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Game</em>' container reference.
   * @see #setGame(Game)
   * @see de.psi.pjf.dart.DartPackage#getParticipation_Game()
   * @see de.psi.pjf.dart.Game#getParticipations
   * @model opposite="participations" transient="false"
   * @generated
   */
  Game getGame();

  /**
   * Sets the value of the '{@link de.psi.pjf.dart.Participation#getGame <em>Game</em>}' container reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Game</em>' container reference.
   * @see #getGame()
   * @generated
   */
  void setGame(Game value);

  /**
   * Returns the value of the '<em><b>Throws</b></em>' containment reference list.
   * The list contents are of type {@link de.psi.pjf.dart.Throw}.
   * It is bidirectional and its opposite is '{@link de.psi.pjf.dart.Throw#getParticipation <em>Participation</em>}'.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Throws</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Throws</em>' containment reference list.
   * @see de.psi.pjf.dart.DartPackage#getParticipation_Throws()
   * @see de.psi.pjf.dart.Throw#getParticipation
   * @model opposite="participation" containment="true"
   * @generated
   */
  EList<Throw> getThrows();

  /**
   * Returns the value of the '<em><b>Player</b></em>' reference.
   * It is bidirectional and its opposite is '{@link de.psi.pjf.dart.Player#getParticipations <em>Participations</em>}'.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Player</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Player</em>' reference.
   * @see #setPlayer(Player)
   * @see de.psi.pjf.dart.DartPackage#getParticipation_Player()
   * @see de.psi.pjf.dart.Player#getParticipations
   * @model opposite="participations"
   * @generated
   */
  Player getPlayer();

  /**
   * Sets the value of the '{@link de.psi.pjf.dart.Participation#getPlayer <em>Player</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Player</em>' reference.
   * @see #getPlayer()
   * @generated
   */
  void setPlayer(Player value);

  /**
   * Returns the value of the '<em><b>To Win</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>To Win</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>To Win</em>' attribute.
   * @see de.psi.pjf.dart.DartPackage#getParticipation_ToWin()
   * @model unique="false" transient="true" changeable="false" volatile="true" derived="true"
   *        annotation="http://www.eclipse.org/emf/2002/GenModel get='<%java.lang.Integer%> _xblockexpression = null;\n{\n\tint _xifexpression = (int) 0;\n\t<%de.psi.pjf.dart.Participation%> _this = this;\n\t<%de.psi.pjf.dart.Game%> _game = _this.getGame();\n\tboolean _equals = <%com.google.common.base.Objects%>.equal(_game, null);\n\tif (_equals)\n\t{\n\t\tint _minus = (-100);\n\t\t_xifexpression = _minus;\n\t}\n\telse\n\t{\n\t\t<%de.psi.pjf.dart.Participation%> _this_1 = this;\n\t\t<%de.psi.pjf.dart.Game%> _game_1 = _this_1.getGame();\n\t\t<%de.psi.pjf.dart.GameType%> _gameType = _game_1.getGameType();\n\t\tint _value = _gameType.getValue();\n\t\t_xifexpression = _value;\n\t}\n\tfinal int s = _xifexpression;\n\t<%de.psi.pjf.dart.Participation%> _this_2 = this;\n\t<%org.eclipse.emf.common.util.EList%><<%de.psi.pjf.dart.Throw%>> _throws = _this_2.getThrows();\n\tfinal <%org.eclipse.xtext.xbase.lib.Functions.Function2%><<%java.lang.Integer%>,<%de.psi.pjf.dart.Throw%>,<%java.lang.Integer%>> _function = new <%org.eclipse.xtext.xbase.lib.Functions.Function2%><<%java.lang.Integer%>,<%de.psi.pjf.dart.Throw%>,<%java.lang.Integer%>>()\n\t{\n\t\tpublic <%java.lang.Integer%> apply(final <%java.lang.Integer%> a, final <%de.psi.pjf.dart.Throw%> b)\n\t\t{\n\t\t\t<%java.lang.Integer%> _xifexpression = null;\n\t\t\tint _total = b.getTotal();\n\t\t\tboolean _greaterThan = (_total > (a).intValue());\n\t\t\tif (_greaterThan)\n\t\t\t{\n\t\t\t\t_xifexpression = a;\n\t\t\t}\n\t\t\telse\n\t\t\t{\n\t\t\t\tint _total_1 = b.getTotal();\n\t\t\t\tint _minus = ((a).intValue() - _total_1);\n\t\t\t\t_xifexpression = <%java.lang.Integer%>.valueOf(_minus);\n\t\t\t}\n\t\t\treturn _xifexpression;\n\t\t}\n\t};\n\t<%java.lang.Integer%> _fold = <%org.eclipse.xtext.xbase.lib.IterableExtensions%>.<<%de.psi.pjf.dart.Throw%>, <%java.lang.Integer%>>fold(_throws, <%java.lang.Integer%>.valueOf(s), _function);\n\t_xblockexpression = (_fold);\n}\nreturn (_xblockexpression).intValue();'"
   * @generated
   */
  int getToWin();

  /**
   * Returns the value of the '<em><b>Game Max</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Game Max</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Game Max</em>' attribute.
   * @see de.psi.pjf.dart.DartPackage#getParticipation_GameMax()
   * @model unique="false" transient="true" changeable="false" volatile="true" derived="true"
   *        annotation="http://www.eclipse.org/emf/2002/GenModel get='<%de.psi.pjf.dart.Participation%> _this = this;\n<%org.eclipse.emf.common.util.EList%><<%de.psi.pjf.dart.Throw%>> _throws = _this.getThrows();\nfinal <%org.eclipse.xtext.xbase.lib.Functions.Function2%><<%java.lang.Integer%>,<%de.psi.pjf.dart.Throw%>,<%java.lang.Integer%>> _function = new <%org.eclipse.xtext.xbase.lib.Functions.Function2%><<%java.lang.Integer%>,<%de.psi.pjf.dart.Throw%>,<%java.lang.Integer%>>()\n{\n\tpublic <%java.lang.Integer%> apply(final <%java.lang.Integer%> a, final <%de.psi.pjf.dart.Throw%> b)\n\t{\n\t\tint _total = b.getTotal();\n\t\tint _max = <%java.lang.Math%>.max((a).intValue(), _total);\n\t\treturn <%java.lang.Integer%>.valueOf(_max);\n\t}\n};\n<%java.lang.Integer%> _fold = <%org.eclipse.xtext.xbase.lib.IterableExtensions%>.<<%de.psi.pjf.dart.Throw%>, <%java.lang.Integer%>>fold(_throws, <%java.lang.Integer%>.valueOf(0), _function);\nreturn (_fold).intValue();'"
   * @generated
   */
  int getGameMax();

  /**
   * Returns the value of the '<em><b>Winner</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Winner</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Winner</em>' attribute.
   * @see de.psi.pjf.dart.DartPackage#getParticipation_Winner()
   * @model unique="false" transient="true" changeable="false" volatile="true" derived="true"
   *        annotation="http://www.eclipse.org/emf/2002/GenModel get='<%de.psi.pjf.dart.Participation%> _this = this;\nint _toWin = _this.getToWin();\nboolean _equals = (_toWin == 0);\nreturn _equals;'"
   * @generated
   */
  boolean isWinner();

  /**
   * Returns the value of the '<em><b>Game Avg</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Game Avg</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Game Avg</em>' attribute.
   * @see de.psi.pjf.dart.DartPackage#getParticipation_GameAvg()
   * @model unique="false" transient="true" changeable="false" volatile="true" derived="true"
   *        annotation="http://www.eclipse.org/emf/2002/GenModel get='double _xifexpression = (double) 0;\n<%de.psi.pjf.dart.Participation%> _this = this;\n<%org.eclipse.emf.common.util.EList%><<%de.psi.pjf.dart.Throw%>> _throws = _this.getThrows();\nint _size = _throws.size();\nboolean _notEquals = (_size != 0);\nif (_notEquals)\n{\n\t<%de.psi.pjf.dart.Participation%> _this_1 = this;\n\t<%org.eclipse.emf.common.util.EList%><<%de.psi.pjf.dart.Throw%>> _throws_1 = _this_1.getThrows();\n\tfinal <%org.eclipse.xtext.xbase.lib.Functions.Function2%><<%java.lang.Integer%>,<%de.psi.pjf.dart.Throw%>,<%java.lang.Integer%>> _function = new <%org.eclipse.xtext.xbase.lib.Functions.Function2%><<%java.lang.Integer%>,<%de.psi.pjf.dart.Throw%>,<%java.lang.Integer%>>()\n\t{\n\t\tpublic <%java.lang.Integer%> apply(final <%java.lang.Integer%> a, final <%de.psi.pjf.dart.Throw%> b)\n\t\t{\n\t\t\tint _total = b.getTotal();\n\t\t\tint _plus = ((a).intValue() + _total);\n\t\t\treturn <%java.lang.Integer%>.valueOf(_plus);\n\t\t}\n\t};\n\t<%java.lang.Integer%> _fold = <%org.eclipse.xtext.xbase.lib.IterableExtensions%>.<<%de.psi.pjf.dart.Throw%>, <%java.lang.Integer%>>fold(_throws_1, <%java.lang.Integer%>.valueOf(0), _function);\n\t<%de.psi.pjf.dart.Participation%> _this_2 = this;\n\t<%org.eclipse.emf.common.util.EList%><<%de.psi.pjf.dart.Throw%>> _throws_2 = _this_2.getThrows();\n\tint _size_1 = _throws_2.size();\n\tint _divide = ((_fold).intValue() / _size_1);\n\t_xifexpression = _divide;\n}\nelse\n{\n\t_xifexpression = 0.0;\n}\nreturn _xifexpression;'"
   * @generated
   */
  double getGameAvg();

} // Participation
