/**
 */
package de.psi.pjf.dart;

import java.util.Date;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Game</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.psi.pjf.dart.Game#getLeague <em>League</em>}</li>
 *   <li>{@link de.psi.pjf.dart.Game#getParticipations <em>Participations</em>}</li>
 *   <li>{@link de.psi.pjf.dart.Game#getGameType <em>Game Type</em>}</li>
 *   <li>{@link de.psi.pjf.dart.Game#getWinners <em>Winners</em>}</li>
 *   <li>{@link de.psi.pjf.dart.Game#getCreated <em>Created</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.psi.pjf.dart.DartPackage#getGame()
 * @model annotation="http://www.eclipse.org/emf/2002/GenModel image='false'"
 * @generated
 */
public interface Game extends EObject
{
  /**
   * Returns the value of the '<em><b>League</b></em>' container reference.
   * It is bidirectional and its opposite is '{@link de.psi.pjf.dart.League#getGames <em>Games</em>}'.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>League</em>' container reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>League</em>' container reference.
   * @see #setLeague(League)
   * @see de.psi.pjf.dart.DartPackage#getGame_League()
   * @see de.psi.pjf.dart.League#getGames
   * @model opposite="games" transient="false"
   * @generated
   */
  League getLeague();

  /**
   * Sets the value of the '{@link de.psi.pjf.dart.Game#getLeague <em>League</em>}' container reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>League</em>' container reference.
   * @see #getLeague()
   * @generated
   */
  void setLeague(League value);

  /**
   * Returns the value of the '<em><b>Participations</b></em>' containment reference list.
   * The list contents are of type {@link de.psi.pjf.dart.Participation}.
   * It is bidirectional and its opposite is '{@link de.psi.pjf.dart.Participation#getGame <em>Game</em>}'.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Participations</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Participations</em>' containment reference list.
   * @see de.psi.pjf.dart.DartPackage#getGame_Participations()
   * @see de.psi.pjf.dart.Participation#getGame
   * @model opposite="game" containment="true"
   * @generated
   */
  EList<Participation> getParticipations();

  /**
   * Returns the value of the '<em><b>Game Type</b></em>' attribute.
   * The literals are from the enumeration {@link de.psi.pjf.dart.GameType}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Game Type</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Game Type</em>' attribute.
   * @see de.psi.pjf.dart.GameType
   * @see #setGameType(GameType)
   * @see de.psi.pjf.dart.DartPackage#getGame_GameType()
   * @model unique="false"
   * @generated
   */
  GameType getGameType();

  /**
   * Sets the value of the '{@link de.psi.pjf.dart.Game#getGameType <em>Game Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Game Type</em>' attribute.
   * @see de.psi.pjf.dart.GameType
   * @see #getGameType()
   * @generated
   */
  void setGameType(GameType value);

  /**
   * Returns the value of the '<em><b>Winners</b></em>' reference list.
   * The list contents are of type {@link de.psi.pjf.dart.Player}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Winners</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Winners</em>' reference list.
   * @see de.psi.pjf.dart.DartPackage#getGame_Winners()
   * @model transient="true" changeable="false" volatile="true" derived="true"
   *        annotation="http://www.eclipse.org/emf/2002/GenModel get='<%de.psi.pjf.dart.Game%> _this = this;\n<%org.eclipse.emf.common.util.EList%><<%de.psi.pjf.dart.Participation%>> _participations = _this.getParticipations();\nfinal <%org.eclipse.xtext.xbase.lib.Functions.Function1%><<%de.psi.pjf.dart.Participation%>,<%java.lang.Boolean%>> _function = new <%org.eclipse.xtext.xbase.lib.Functions.Function1%><<%de.psi.pjf.dart.Participation%>,<%java.lang.Boolean%>>()\n{\n\tpublic <%java.lang.Boolean%> apply(final <%de.psi.pjf.dart.Participation%> it)\n\t{\n\t\tboolean _isWinner = it.isWinner();\n\t\treturn <%java.lang.Boolean%>.valueOf(_isWinner);\n\t}\n};\n<%java.lang.Iterable%><<%de.psi.pjf.dart.Participation%>> _filter = <%org.eclipse.xtext.xbase.lib.IterableExtensions%>.<<%de.psi.pjf.dart.Participation%>>filter(_participations, _function);\nfinal <%org.eclipse.xtext.xbase.lib.Functions.Function1%><<%de.psi.pjf.dart.Participation%>,<%de.psi.pjf.dart.Player%>> _function_1 = new <%org.eclipse.xtext.xbase.lib.Functions.Function1%><<%de.psi.pjf.dart.Participation%>,<%de.psi.pjf.dart.Player%>>()\n{\n\tpublic <%de.psi.pjf.dart.Player%> apply(final <%de.psi.pjf.dart.Participation%> it)\n\t{\n\t\t<%de.psi.pjf.dart.Player%> _player = it.getPlayer();\n\t\treturn _player;\n\t}\n};\n<%java.lang.Iterable%><<%de.psi.pjf.dart.Player%>> _map = <%org.eclipse.xtext.xbase.lib.IterableExtensions%>.<<%de.psi.pjf.dart.Participation%>, <%de.psi.pjf.dart.Player%>>map(_filter, _function_1);\n<%org.eclipse.emf.common.util.EList%><<%de.psi.pjf.dart.Player%>> _eList = <%org.eclipse.emf.common.util.ECollections%>.<<%de.psi.pjf.dart.Player%>>toEList(_map);\nreturn _eList;'"
   * @generated
   */
  EList<Player> getWinners();

  /**
   * Returns the value of the '<em><b>Created</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Created</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Created</em>' attribute.
   * @see #setCreated(Date)
   * @see de.psi.pjf.dart.DartPackage#getGame_Created()
   * @model unique="false"
   * @generated
   */
  Date getCreated();

  /**
   * Sets the value of the '{@link de.psi.pjf.dart.Game#getCreated <em>Created</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Created</em>' attribute.
   * @see #getCreated()
   * @generated
   */
  void setCreated(Date value);

} // Game
