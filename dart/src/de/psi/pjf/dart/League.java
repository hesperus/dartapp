/**
 */
package de.psi.pjf.dart;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>League</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.psi.pjf.dart.League#getName <em>Name</em>}</li>
 *   <li>{@link de.psi.pjf.dart.League#getPlayers <em>Players</em>}</li>
 *   <li>{@link de.psi.pjf.dart.League#getGames <em>Games</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.psi.pjf.dart.DartPackage#getLeague()
 * @model annotation="http://www.eclipse.org/emf/2002/GenModel image='false'"
 * @generated
 */
public interface League extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see de.psi.pjf.dart.DartPackage#getLeague_Name()
   * @model unique="false" id="true"
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link de.psi.pjf.dart.League#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Players</b></em>' containment reference list.
   * The list contents are of type {@link de.psi.pjf.dart.Player}.
   * It is bidirectional and its opposite is '{@link de.psi.pjf.dart.Player#getLeague <em>League</em>}'.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Players</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Players</em>' containment reference list.
   * @see de.psi.pjf.dart.DartPackage#getLeague_Players()
   * @see de.psi.pjf.dart.Player#getLeague
   * @model opposite="league" containment="true" ordered="false"
   * @generated
   */
  EList<Player> getPlayers();

  /**
   * Returns the value of the '<em><b>Games</b></em>' containment reference list.
   * The list contents are of type {@link de.psi.pjf.dart.Game}.
   * It is bidirectional and its opposite is '{@link de.psi.pjf.dart.Game#getLeague <em>League</em>}'.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Games</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Games</em>' containment reference list.
   * @see de.psi.pjf.dart.DartPackage#getLeague_Games()
   * @see de.psi.pjf.dart.Game#getLeague
   * @model opposite="league" containment="true"
   * @generated
   */
  EList<Game> getGames();

} // League
