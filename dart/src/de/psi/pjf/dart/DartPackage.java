/**
 */
package de.psi.pjf.dart;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.psi.pjf.dart.DartFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel editorDirectory='/dart.editor/src' editDirectory='/dart.edit/src' creationSubmenus='true' modelDirectory='/dart/src' creationIcons='false' basePackage='de.psi.pjf'"
 *        annotation="http://www.eclipse.org/emf/2011/Xcore Fgh='sdhfosdfsdf'"
 * @generated
 */
public interface DartPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "dart";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "de.psi.pjf.dart";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "dart";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  DartPackage eINSTANCE = de.psi.pjf.dart.impl.DartPackageImpl.init();

  /**
   * The meta object id for the '{@link de.psi.pjf.dart.impl.LeagueImpl <em>League</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.psi.pjf.dart.impl.LeagueImpl
   * @see de.psi.pjf.dart.impl.DartPackageImpl#getLeague()
   * @generated
   */
  int LEAGUE = 0;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LEAGUE__NAME = 0;

  /**
   * The feature id for the '<em><b>Players</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LEAGUE__PLAYERS = 1;

  /**
   * The feature id for the '<em><b>Games</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LEAGUE__GAMES = 2;

  /**
   * The number of structural features of the '<em>League</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LEAGUE_FEATURE_COUNT = 3;

  /**
   * The number of operations of the '<em>League</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LEAGUE_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link de.psi.pjf.dart.impl.PlayerImpl <em>Player</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.psi.pjf.dart.impl.PlayerImpl
   * @see de.psi.pjf.dart.impl.DartPackageImpl#getPlayer()
   * @generated
   */
  int PLAYER = 1;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PLAYER__NAME = 0;

  /**
   * The feature id for the '<em><b>League</b></em>' container reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PLAYER__LEAGUE = 1;

  /**
   * The feature id for the '<em><b>Participations</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PLAYER__PARTICIPATIONS = 2;

  /**
   * The feature id for the '<em><b>Win Count</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PLAYER__WIN_COUNT = 3;

  /**
   * The feature id for the '<em><b>Best Throw</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PLAYER__BEST_THROW = 4;

  /**
   * The feature id for the '<em><b>Game Count</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PLAYER__GAME_COUNT = 5;

  /**
   * The feature id for the '<em><b>Throws Avg</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PLAYER__THROWS_AVG = 6;

  /**
   * The number of structural features of the '<em>Player</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PLAYER_FEATURE_COUNT = 7;

  /**
   * The number of operations of the '<em>Player</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PLAYER_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link de.psi.pjf.dart.impl.GameImpl <em>Game</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.psi.pjf.dart.impl.GameImpl
   * @see de.psi.pjf.dart.impl.DartPackageImpl#getGame()
   * @generated
   */
  int GAME = 2;

  /**
   * The feature id for the '<em><b>League</b></em>' container reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GAME__LEAGUE = 0;

  /**
   * The feature id for the '<em><b>Participations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GAME__PARTICIPATIONS = 1;

  /**
   * The feature id for the '<em><b>Game Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GAME__GAME_TYPE = 2;

  /**
   * The feature id for the '<em><b>Winners</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GAME__WINNERS = 3;

  /**
   * The feature id for the '<em><b>Created</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GAME__CREATED = 4;

  /**
   * The number of structural features of the '<em>Game</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GAME_FEATURE_COUNT = 5;

  /**
   * The number of operations of the '<em>Game</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GAME_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link de.psi.pjf.dart.impl.ParticipationImpl <em>Participation</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.psi.pjf.dart.impl.ParticipationImpl
   * @see de.psi.pjf.dart.impl.DartPackageImpl#getParticipation()
   * @generated
   */
  int PARTICIPATION = 3;

  /**
   * The feature id for the '<em><b>Game</b></em>' container reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARTICIPATION__GAME = 0;

  /**
   * The feature id for the '<em><b>Throws</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARTICIPATION__THROWS = 1;

  /**
   * The feature id for the '<em><b>Player</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARTICIPATION__PLAYER = 2;

  /**
   * The feature id for the '<em><b>To Win</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARTICIPATION__TO_WIN = 3;

  /**
   * The feature id for the '<em><b>Game Max</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARTICIPATION__GAME_MAX = 4;

  /**
   * The feature id for the '<em><b>Winner</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARTICIPATION__WINNER = 5;

  /**
   * The feature id for the '<em><b>Game Avg</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARTICIPATION__GAME_AVG = 6;

  /**
   * The number of structural features of the '<em>Participation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARTICIPATION_FEATURE_COUNT = 7;

  /**
   * The number of operations of the '<em>Participation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARTICIPATION_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link de.psi.pjf.dart.impl.ThrowImpl <em>Throw</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.psi.pjf.dart.impl.ThrowImpl
   * @see de.psi.pjf.dart.impl.DartPackageImpl#getThrow()
   * @generated
   */
  int THROW = 4;

  /**
   * The feature id for the '<em><b>Participation</b></em>' container reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THROW__PARTICIPATION = 0;

  /**
   * The feature id for the '<em><b>First</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THROW__FIRST = 1;

  /**
   * The feature id for the '<em><b>First Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THROW__FIRST_TYPE = 2;

  /**
   * The feature id for the '<em><b>Second</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THROW__SECOND = 3;

  /**
   * The feature id for the '<em><b>Second Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THROW__SECOND_TYPE = 4;

  /**
   * The feature id for the '<em><b>Third</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THROW__THIRD = 5;

  /**
   * The feature id for the '<em><b>Third Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THROW__THIRD_TYPE = 6;

  /**
   * The feature id for the '<em><b>Total</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THROW__TOTAL = 7;

  /**
   * The feature id for the '<em><b>Doubles</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THROW__DOUBLES = 8;

  /**
   * The feature id for the '<em><b>Triples</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THROW__TRIPLES = 9;

  /**
   * The feature id for the '<em><b>Best</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THROW__BEST = 10;

  /**
   * The number of structural features of the '<em>Throw</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THROW_FEATURE_COUNT = 11;

  /**
   * The number of operations of the '<em>Throw</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THROW_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link de.psi.pjf.dart.ThrowType <em>Throw Type</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.psi.pjf.dart.ThrowType
   * @see de.psi.pjf.dart.impl.DartPackageImpl#getThrowType()
   * @generated
   */
  int THROW_TYPE = 5;

  /**
   * The meta object id for the '{@link de.psi.pjf.dart.GameType <em>Game Type</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.psi.pjf.dart.GameType
   * @see de.psi.pjf.dart.impl.DartPackageImpl#getGameType()
   * @generated
   */
  int GAME_TYPE = 6;


  /**
   * Returns the meta object for class '{@link de.psi.pjf.dart.League <em>League</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>League</em>'.
   * @see de.psi.pjf.dart.League
   * @generated
   */
  EClass getLeague();

  /**
   * Returns the meta object for the attribute '{@link de.psi.pjf.dart.League#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see de.psi.pjf.dart.League#getName()
   * @see #getLeague()
   * @generated
   */
  EAttribute getLeague_Name();

  /**
   * Returns the meta object for the containment reference list '{@link de.psi.pjf.dart.League#getPlayers <em>Players</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Players</em>'.
   * @see de.psi.pjf.dart.League#getPlayers()
   * @see #getLeague()
   * @generated
   */
  EReference getLeague_Players();

  /**
   * Returns the meta object for the containment reference list '{@link de.psi.pjf.dart.League#getGames <em>Games</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Games</em>'.
   * @see de.psi.pjf.dart.League#getGames()
   * @see #getLeague()
   * @generated
   */
  EReference getLeague_Games();

  /**
   * Returns the meta object for class '{@link de.psi.pjf.dart.Player <em>Player</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Player</em>'.
   * @see de.psi.pjf.dart.Player
   * @generated
   */
  EClass getPlayer();

  /**
   * Returns the meta object for the attribute '{@link de.psi.pjf.dart.Player#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see de.psi.pjf.dart.Player#getName()
   * @see #getPlayer()
   * @generated
   */
  EAttribute getPlayer_Name();

  /**
   * Returns the meta object for the container reference '{@link de.psi.pjf.dart.Player#getLeague <em>League</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the container reference '<em>League</em>'.
   * @see de.psi.pjf.dart.Player#getLeague()
   * @see #getPlayer()
   * @generated
   */
  EReference getPlayer_League();

  /**
   * Returns the meta object for the reference list '{@link de.psi.pjf.dart.Player#getParticipations <em>Participations</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Participations</em>'.
   * @see de.psi.pjf.dart.Player#getParticipations()
   * @see #getPlayer()
   * @generated
   */
  EReference getPlayer_Participations();

  /**
   * Returns the meta object for the attribute '{@link de.psi.pjf.dart.Player#getWinCount <em>Win Count</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Win Count</em>'.
   * @see de.psi.pjf.dart.Player#getWinCount()
   * @see #getPlayer()
   * @generated
   */
  EAttribute getPlayer_WinCount();

  /**
   * Returns the meta object for the attribute '{@link de.psi.pjf.dart.Player#getBestThrow <em>Best Throw</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Best Throw</em>'.
   * @see de.psi.pjf.dart.Player#getBestThrow()
   * @see #getPlayer()
   * @generated
   */
  EAttribute getPlayer_BestThrow();

  /**
   * Returns the meta object for the attribute '{@link de.psi.pjf.dart.Player#getGameCount <em>Game Count</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Game Count</em>'.
   * @see de.psi.pjf.dart.Player#getGameCount()
   * @see #getPlayer()
   * @generated
   */
  EAttribute getPlayer_GameCount();

  /**
   * Returns the meta object for the attribute '{@link de.psi.pjf.dart.Player#getThrowsAvg <em>Throws Avg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Throws Avg</em>'.
   * @see de.psi.pjf.dart.Player#getThrowsAvg()
   * @see #getPlayer()
   * @generated
   */
  EAttribute getPlayer_ThrowsAvg();

  /**
   * Returns the meta object for class '{@link de.psi.pjf.dart.Game <em>Game</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Game</em>'.
   * @see de.psi.pjf.dart.Game
   * @generated
   */
  EClass getGame();

  /**
   * Returns the meta object for the container reference '{@link de.psi.pjf.dart.Game#getLeague <em>League</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the container reference '<em>League</em>'.
   * @see de.psi.pjf.dart.Game#getLeague()
   * @see #getGame()
   * @generated
   */
  EReference getGame_League();

  /**
   * Returns the meta object for the containment reference list '{@link de.psi.pjf.dart.Game#getParticipations <em>Participations</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Participations</em>'.
   * @see de.psi.pjf.dart.Game#getParticipations()
   * @see #getGame()
   * @generated
   */
  EReference getGame_Participations();

  /**
   * Returns the meta object for the attribute '{@link de.psi.pjf.dart.Game#getGameType <em>Game Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Game Type</em>'.
   * @see de.psi.pjf.dart.Game#getGameType()
   * @see #getGame()
   * @generated
   */
  EAttribute getGame_GameType();

  /**
   * Returns the meta object for the reference list '{@link de.psi.pjf.dart.Game#getWinners <em>Winners</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Winners</em>'.
   * @see de.psi.pjf.dart.Game#getWinners()
   * @see #getGame()
   * @generated
   */
  EReference getGame_Winners();

  /**
   * Returns the meta object for the attribute '{@link de.psi.pjf.dart.Game#getCreated <em>Created</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Created</em>'.
   * @see de.psi.pjf.dart.Game#getCreated()
   * @see #getGame()
   * @generated
   */
  EAttribute getGame_Created();

  /**
   * Returns the meta object for class '{@link de.psi.pjf.dart.Participation <em>Participation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Participation</em>'.
   * @see de.psi.pjf.dart.Participation
   * @generated
   */
  EClass getParticipation();

  /**
   * Returns the meta object for the container reference '{@link de.psi.pjf.dart.Participation#getGame <em>Game</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the container reference '<em>Game</em>'.
   * @see de.psi.pjf.dart.Participation#getGame()
   * @see #getParticipation()
   * @generated
   */
  EReference getParticipation_Game();

  /**
   * Returns the meta object for the containment reference list '{@link de.psi.pjf.dart.Participation#getThrows <em>Throws</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Throws</em>'.
   * @see de.psi.pjf.dart.Participation#getThrows()
   * @see #getParticipation()
   * @generated
   */
  EReference getParticipation_Throws();

  /**
   * Returns the meta object for the reference '{@link de.psi.pjf.dart.Participation#getPlayer <em>Player</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Player</em>'.
   * @see de.psi.pjf.dart.Participation#getPlayer()
   * @see #getParticipation()
   * @generated
   */
  EReference getParticipation_Player();

  /**
   * Returns the meta object for the attribute '{@link de.psi.pjf.dart.Participation#getToWin <em>To Win</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>To Win</em>'.
   * @see de.psi.pjf.dart.Participation#getToWin()
   * @see #getParticipation()
   * @generated
   */
  EAttribute getParticipation_ToWin();

  /**
   * Returns the meta object for the attribute '{@link de.psi.pjf.dart.Participation#getGameMax <em>Game Max</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Game Max</em>'.
   * @see de.psi.pjf.dart.Participation#getGameMax()
   * @see #getParticipation()
   * @generated
   */
  EAttribute getParticipation_GameMax();

  /**
   * Returns the meta object for the attribute '{@link de.psi.pjf.dart.Participation#isWinner <em>Winner</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Winner</em>'.
   * @see de.psi.pjf.dart.Participation#isWinner()
   * @see #getParticipation()
   * @generated
   */
  EAttribute getParticipation_Winner();

  /**
   * Returns the meta object for the attribute '{@link de.psi.pjf.dart.Participation#getGameAvg <em>Game Avg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Game Avg</em>'.
   * @see de.psi.pjf.dart.Participation#getGameAvg()
   * @see #getParticipation()
   * @generated
   */
  EAttribute getParticipation_GameAvg();

  /**
   * Returns the meta object for class '{@link de.psi.pjf.dart.Throw <em>Throw</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Throw</em>'.
   * @see de.psi.pjf.dart.Throw
   * @generated
   */
  EClass getThrow();

  /**
   * Returns the meta object for the container reference '{@link de.psi.pjf.dart.Throw#getParticipation <em>Participation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the container reference '<em>Participation</em>'.
   * @see de.psi.pjf.dart.Throw#getParticipation()
   * @see #getThrow()
   * @generated
   */
  EReference getThrow_Participation();

  /**
   * Returns the meta object for the attribute '{@link de.psi.pjf.dart.Throw#getFirst <em>First</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>First</em>'.
   * @see de.psi.pjf.dart.Throw#getFirst()
   * @see #getThrow()
   * @generated
   */
  EAttribute getThrow_First();

  /**
   * Returns the meta object for the attribute '{@link de.psi.pjf.dart.Throw#getFirstType <em>First Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>First Type</em>'.
   * @see de.psi.pjf.dart.Throw#getFirstType()
   * @see #getThrow()
   * @generated
   */
  EAttribute getThrow_FirstType();

  /**
   * Returns the meta object for the attribute '{@link de.psi.pjf.dart.Throw#getSecond <em>Second</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Second</em>'.
   * @see de.psi.pjf.dart.Throw#getSecond()
   * @see #getThrow()
   * @generated
   */
  EAttribute getThrow_Second();

  /**
   * Returns the meta object for the attribute '{@link de.psi.pjf.dart.Throw#getSecondType <em>Second Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Second Type</em>'.
   * @see de.psi.pjf.dart.Throw#getSecondType()
   * @see #getThrow()
   * @generated
   */
  EAttribute getThrow_SecondType();

  /**
   * Returns the meta object for the attribute '{@link de.psi.pjf.dart.Throw#getThird <em>Third</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Third</em>'.
   * @see de.psi.pjf.dart.Throw#getThird()
   * @see #getThrow()
   * @generated
   */
  EAttribute getThrow_Third();

  /**
   * Returns the meta object for the attribute '{@link de.psi.pjf.dart.Throw#getThirdType <em>Third Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Third Type</em>'.
   * @see de.psi.pjf.dart.Throw#getThirdType()
   * @see #getThrow()
   * @generated
   */
  EAttribute getThrow_ThirdType();

  /**
   * Returns the meta object for the attribute '{@link de.psi.pjf.dart.Throw#getTotal <em>Total</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Total</em>'.
   * @see de.psi.pjf.dart.Throw#getTotal()
   * @see #getThrow()
   * @generated
   */
  EAttribute getThrow_Total();

  /**
   * Returns the meta object for the attribute '{@link de.psi.pjf.dart.Throw#getDoubles <em>Doubles</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Doubles</em>'.
   * @see de.psi.pjf.dart.Throw#getDoubles()
   * @see #getThrow()
   * @generated
   */
  EAttribute getThrow_Doubles();

  /**
   * Returns the meta object for the attribute '{@link de.psi.pjf.dart.Throw#getTriples <em>Triples</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Triples</em>'.
   * @see de.psi.pjf.dart.Throw#getTriples()
   * @see #getThrow()
   * @generated
   */
  EAttribute getThrow_Triples();

  /**
   * Returns the meta object for the attribute '{@link de.psi.pjf.dart.Throw#isBest <em>Best</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Best</em>'.
   * @see de.psi.pjf.dart.Throw#isBest()
   * @see #getThrow()
   * @generated
   */
  EAttribute getThrow_Best();

  /**
   * Returns the meta object for enum '{@link de.psi.pjf.dart.ThrowType <em>Throw Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Throw Type</em>'.
   * @see de.psi.pjf.dart.ThrowType
   * @generated
   */
  EEnum getThrowType();

  /**
   * Returns the meta object for enum '{@link de.psi.pjf.dart.GameType <em>Game Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Game Type</em>'.
   * @see de.psi.pjf.dart.GameType
   * @generated
   */
  EEnum getGameType();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  DartFactory getDartFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each operation of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link de.psi.pjf.dart.impl.LeagueImpl <em>League</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see de.psi.pjf.dart.impl.LeagueImpl
     * @see de.psi.pjf.dart.impl.DartPackageImpl#getLeague()
     * @generated
     */
    EClass LEAGUE = eINSTANCE.getLeague();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute LEAGUE__NAME = eINSTANCE.getLeague_Name();

    /**
     * The meta object literal for the '<em><b>Players</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LEAGUE__PLAYERS = eINSTANCE.getLeague_Players();

    /**
     * The meta object literal for the '<em><b>Games</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LEAGUE__GAMES = eINSTANCE.getLeague_Games();

    /**
     * The meta object literal for the '{@link de.psi.pjf.dart.impl.PlayerImpl <em>Player</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see de.psi.pjf.dart.impl.PlayerImpl
     * @see de.psi.pjf.dart.impl.DartPackageImpl#getPlayer()
     * @generated
     */
    EClass PLAYER = eINSTANCE.getPlayer();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PLAYER__NAME = eINSTANCE.getPlayer_Name();

    /**
     * The meta object literal for the '<em><b>League</b></em>' container reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PLAYER__LEAGUE = eINSTANCE.getPlayer_League();

    /**
     * The meta object literal for the '<em><b>Participations</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PLAYER__PARTICIPATIONS = eINSTANCE.getPlayer_Participations();

    /**
     * The meta object literal for the '<em><b>Win Count</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PLAYER__WIN_COUNT = eINSTANCE.getPlayer_WinCount();

    /**
     * The meta object literal for the '<em><b>Best Throw</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PLAYER__BEST_THROW = eINSTANCE.getPlayer_BestThrow();

    /**
     * The meta object literal for the '<em><b>Game Count</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PLAYER__GAME_COUNT = eINSTANCE.getPlayer_GameCount();

    /**
     * The meta object literal for the '<em><b>Throws Avg</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PLAYER__THROWS_AVG = eINSTANCE.getPlayer_ThrowsAvg();

    /**
     * The meta object literal for the '{@link de.psi.pjf.dart.impl.GameImpl <em>Game</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see de.psi.pjf.dart.impl.GameImpl
     * @see de.psi.pjf.dart.impl.DartPackageImpl#getGame()
     * @generated
     */
    EClass GAME = eINSTANCE.getGame();

    /**
     * The meta object literal for the '<em><b>League</b></em>' container reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GAME__LEAGUE = eINSTANCE.getGame_League();

    /**
     * The meta object literal for the '<em><b>Participations</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GAME__PARTICIPATIONS = eINSTANCE.getGame_Participations();

    /**
     * The meta object literal for the '<em><b>Game Type</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute GAME__GAME_TYPE = eINSTANCE.getGame_GameType();

    /**
     * The meta object literal for the '<em><b>Winners</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GAME__WINNERS = eINSTANCE.getGame_Winners();

    /**
     * The meta object literal for the '<em><b>Created</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute GAME__CREATED = eINSTANCE.getGame_Created();

    /**
     * The meta object literal for the '{@link de.psi.pjf.dart.impl.ParticipationImpl <em>Participation</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see de.psi.pjf.dart.impl.ParticipationImpl
     * @see de.psi.pjf.dart.impl.DartPackageImpl#getParticipation()
     * @generated
     */
    EClass PARTICIPATION = eINSTANCE.getParticipation();

    /**
     * The meta object literal for the '<em><b>Game</b></em>' container reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PARTICIPATION__GAME = eINSTANCE.getParticipation_Game();

    /**
     * The meta object literal for the '<em><b>Throws</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PARTICIPATION__THROWS = eINSTANCE.getParticipation_Throws();

    /**
     * The meta object literal for the '<em><b>Player</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PARTICIPATION__PLAYER = eINSTANCE.getParticipation_Player();

    /**
     * The meta object literal for the '<em><b>To Win</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PARTICIPATION__TO_WIN = eINSTANCE.getParticipation_ToWin();

    /**
     * The meta object literal for the '<em><b>Game Max</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PARTICIPATION__GAME_MAX = eINSTANCE.getParticipation_GameMax();

    /**
     * The meta object literal for the '<em><b>Winner</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PARTICIPATION__WINNER = eINSTANCE.getParticipation_Winner();

    /**
     * The meta object literal for the '<em><b>Game Avg</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PARTICIPATION__GAME_AVG = eINSTANCE.getParticipation_GameAvg();

    /**
     * The meta object literal for the '{@link de.psi.pjf.dart.impl.ThrowImpl <em>Throw</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see de.psi.pjf.dart.impl.ThrowImpl
     * @see de.psi.pjf.dart.impl.DartPackageImpl#getThrow()
     * @generated
     */
    EClass THROW = eINSTANCE.getThrow();

    /**
     * The meta object literal for the '<em><b>Participation</b></em>' container reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference THROW__PARTICIPATION = eINSTANCE.getThrow_Participation();

    /**
     * The meta object literal for the '<em><b>First</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute THROW__FIRST = eINSTANCE.getThrow_First();

    /**
     * The meta object literal for the '<em><b>First Type</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute THROW__FIRST_TYPE = eINSTANCE.getThrow_FirstType();

    /**
     * The meta object literal for the '<em><b>Second</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute THROW__SECOND = eINSTANCE.getThrow_Second();

    /**
     * The meta object literal for the '<em><b>Second Type</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute THROW__SECOND_TYPE = eINSTANCE.getThrow_SecondType();

    /**
     * The meta object literal for the '<em><b>Third</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute THROW__THIRD = eINSTANCE.getThrow_Third();

    /**
     * The meta object literal for the '<em><b>Third Type</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute THROW__THIRD_TYPE = eINSTANCE.getThrow_ThirdType();

    /**
     * The meta object literal for the '<em><b>Total</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute THROW__TOTAL = eINSTANCE.getThrow_Total();

    /**
     * The meta object literal for the '<em><b>Doubles</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute THROW__DOUBLES = eINSTANCE.getThrow_Doubles();

    /**
     * The meta object literal for the '<em><b>Triples</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute THROW__TRIPLES = eINSTANCE.getThrow_Triples();

    /**
     * The meta object literal for the '<em><b>Best</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute THROW__BEST = eINSTANCE.getThrow_Best();

    /**
     * The meta object literal for the '{@link de.psi.pjf.dart.ThrowType <em>Throw Type</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see de.psi.pjf.dart.ThrowType
     * @see de.psi.pjf.dart.impl.DartPackageImpl#getThrowType()
     * @generated
     */
    EEnum THROW_TYPE = eINSTANCE.getThrowType();

    /**
     * The meta object literal for the '{@link de.psi.pjf.dart.GameType <em>Game Type</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see de.psi.pjf.dart.GameType
     * @see de.psi.pjf.dart.impl.DartPackageImpl#getGameType()
     * @generated
     */
    EEnum GAME_TYPE = eINSTANCE.getGameType();

  }

} //DartPackage
