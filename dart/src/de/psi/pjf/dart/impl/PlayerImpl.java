/**
 */
package de.psi.pjf.dart.impl;

import de.psi.pjf.dart.DartPackage;
import de.psi.pjf.dart.League;
import de.psi.pjf.dart.Participation;
import de.psi.pjf.dart.Player;

import de.psi.pjf.dart.Throw;
import java.lang.Iterable;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.Functions.Function2;

import org.eclipse.xtext.xbase.lib.IterableExtensions;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Player</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.psi.pjf.dart.impl.PlayerImpl#getName <em>Name</em>}</li>
 *   <li>{@link de.psi.pjf.dart.impl.PlayerImpl#getLeague <em>League</em>}</li>
 *   <li>{@link de.psi.pjf.dart.impl.PlayerImpl#getParticipations <em>Participations</em>}</li>
 *   <li>{@link de.psi.pjf.dart.impl.PlayerImpl#getWinCount <em>Win Count</em>}</li>
 *   <li>{@link de.psi.pjf.dart.impl.PlayerImpl#getBestThrow <em>Best Throw</em>}</li>
 *   <li>{@link de.psi.pjf.dart.impl.PlayerImpl#getGameCount <em>Game Count</em>}</li>
 *   <li>{@link de.psi.pjf.dart.impl.PlayerImpl#getThrowsAvg <em>Throws Avg</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PlayerImpl extends MinimalEObjectImpl.Container implements Player
{
  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The cached value of the '{@link #getParticipations() <em>Participations</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParticipations()
   * @generated
   * @ordered
   */
  protected EList<Participation> participations;

  /**
   * The default value of the '{@link #getWinCount() <em>Win Count</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getWinCount()
   * @generated
   * @ordered
   */
  protected static final int WIN_COUNT_EDEFAULT = 0;

  /**
   * The default value of the '{@link #getBestThrow() <em>Best Throw</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBestThrow()
   * @generated
   * @ordered
   */
  protected static final int BEST_THROW_EDEFAULT = 0;

  /**
   * The default value of the '{@link #getGameCount() <em>Game Count</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGameCount()
   * @generated
   * @ordered
   */
  protected static final int GAME_COUNT_EDEFAULT = 0;

  /**
   * The default value of the '{@link #getThrowsAvg() <em>Throws Avg</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getThrowsAvg()
   * @generated
   * @ordered
   */
  protected static final double THROWS_AVG_EDEFAULT = 0.0;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected PlayerImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return DartPackage.Literals.PLAYER;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DartPackage.PLAYER__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public League getLeague()
  {
    if (eContainerFeatureID() != DartPackage.PLAYER__LEAGUE) return null;
    return (League)eContainer();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public League basicGetLeague()
  {
    if (eContainerFeatureID() != DartPackage.PLAYER__LEAGUE) return null;
    return (League)eInternalContainer();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetLeague(League newLeague, NotificationChain msgs)
  {
    msgs = eBasicSetContainer((InternalEObject)newLeague, DartPackage.PLAYER__LEAGUE, msgs);
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLeague(League newLeague)
  {
    if (newLeague != eInternalContainer() || (eContainerFeatureID() != DartPackage.PLAYER__LEAGUE && newLeague != null))
    {
      if (EcoreUtil.isAncestor(this, newLeague))
        throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
      NotificationChain msgs = null;
      if (eInternalContainer() != null)
        msgs = eBasicRemoveFromContainer(msgs);
      if (newLeague != null)
        msgs = ((InternalEObject)newLeague).eInverseAdd(this, DartPackage.LEAGUE__PLAYERS, League.class, msgs);
      msgs = basicSetLeague(newLeague, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DartPackage.PLAYER__LEAGUE, newLeague, newLeague));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Participation> getParticipations()
  {
    if (participations == null)
    {
      participations = new EObjectWithInverseResolvingEList<Participation>(Participation.class, this, DartPackage.PLAYER__PARTICIPATIONS, DartPackage.PARTICIPATION__PLAYER);
    }
    return participations;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getWinCount()
  {
    Player _this = this;
    EList<Participation> _participations = _this.getParticipations();
    final Function1<Participation,Boolean> _function = new Function1<Participation,Boolean>()
    {
      public Boolean apply(final Participation it)
      {
        boolean _isWinner = it.isWinner();
        return Boolean.valueOf(_isWinner);
      }
    };
    Iterable<Participation> _filter = IterableExtensions.<Participation>filter(_participations, _function);
    int _size = IterableExtensions.size(_filter);
    return _size;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getBestThrow()
  {
    Player _this = this;
    EList<Participation> _participations = _this.getParticipations();
    final Function2<Integer,Participation,Integer> _function = new Function2<Integer,Participation,Integer>()
    {
      public Integer apply(final Integer a, final Participation b)
      {
        int _gameMax = b.getGameMax();
        int _max = Math.max((a).intValue(), _gameMax);
        return Integer.valueOf(_max);
      }
    };
    Integer _fold = IterableExtensions.<Participation, Integer>fold(_participations, Integer.valueOf(0), _function);
    return (_fold).intValue();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getGameCount()
  {
    Player _this = this;
    EList<Participation> _participations = _this.getParticipations();
    int _size = _participations.size();
    return _size;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public double getThrowsAvg()
  {
    double _xblockexpression = (double) 0;
    {
      Player _this = this;
      EList<Participation> _participations = _this.getParticipations();
      final Function2<Integer,Participation,Integer> _function = new Function2<Integer,Participation,Integer>()
      {
        public Integer apply(final Integer a, final Participation b)
        {
          EList<Throw> _throws = b.getThrows();
          final Function2<Integer,Throw,Integer> _function = new Function2<Integer,Throw,Integer>()
          {
            public Integer apply(final Integer c, final Throw d)
            {
              int _total = d.getTotal();
              int _plus = (_total + (c).intValue());
              return Integer.valueOf(_plus);
            }
          };
          Integer _fold = IterableExtensions.<Throw, Integer>fold(_throws, Integer.valueOf(0), _function);
          int _plus = ((a).intValue() + (_fold).intValue());
          return Integer.valueOf(_plus);
        }
      };
      final Integer up = IterableExtensions.<Participation, Integer>fold(_participations, Integer.valueOf(0), _function);
      Player _this_1 = this;
      EList<Participation> _participations_1 = _this_1.getParticipations();
      final Function2<Integer,Participation,Integer> _function_1 = new Function2<Integer,Participation,Integer>()
      {
        public Integer apply(final Integer a, final Participation b)
        {
          EList<Throw> _throws = b.getThrows();
          int _size = _throws.size();
          int _plus = ((a).intValue() + _size);
          return Integer.valueOf(_plus);
        }
      };
      Integer down = IterableExtensions.<Participation, Integer>fold(_participations_1, Integer.valueOf(0), _function_1);
      double _xifexpression = (double) 0;
      boolean _equals = ((down).intValue() == 0.0);
      if (_equals)
      {
        _xifexpression = 0.0;
      }
      else
      {
        int _divide = ((up).intValue() / (down).intValue());
        _xifexpression = _divide;
      }
      _xblockexpression = (_xifexpression);
    }
    return _xblockexpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case DartPackage.PLAYER__LEAGUE:
        if (eInternalContainer() != null)
          msgs = eBasicRemoveFromContainer(msgs);
        return basicSetLeague((League)otherEnd, msgs);
      case DartPackage.PLAYER__PARTICIPATIONS:
        return ((InternalEList<InternalEObject>)(InternalEList<?>)getParticipations()).basicAdd(otherEnd, msgs);
    }
    return super.eInverseAdd(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case DartPackage.PLAYER__LEAGUE:
        return basicSetLeague(null, msgs);
      case DartPackage.PLAYER__PARTICIPATIONS:
        return ((InternalEList<?>)getParticipations()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs)
  {
    switch (eContainerFeatureID())
    {
      case DartPackage.PLAYER__LEAGUE:
        return eInternalContainer().eInverseRemove(this, DartPackage.LEAGUE__PLAYERS, League.class, msgs);
    }
    return super.eBasicRemoveFromContainerFeature(msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case DartPackage.PLAYER__NAME:
        return getName();
      case DartPackage.PLAYER__LEAGUE:
        if (resolve) return getLeague();
        return basicGetLeague();
      case DartPackage.PLAYER__PARTICIPATIONS:
        return getParticipations();
      case DartPackage.PLAYER__WIN_COUNT:
        return getWinCount();
      case DartPackage.PLAYER__BEST_THROW:
        return getBestThrow();
      case DartPackage.PLAYER__GAME_COUNT:
        return getGameCount();
      case DartPackage.PLAYER__THROWS_AVG:
        return getThrowsAvg();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case DartPackage.PLAYER__NAME:
        setName((String)newValue);
        return;
      case DartPackage.PLAYER__LEAGUE:
        setLeague((League)newValue);
        return;
      case DartPackage.PLAYER__PARTICIPATIONS:
        getParticipations().clear();
        getParticipations().addAll((Collection<? extends Participation>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case DartPackage.PLAYER__NAME:
        setName(NAME_EDEFAULT);
        return;
      case DartPackage.PLAYER__LEAGUE:
        setLeague((League)null);
        return;
      case DartPackage.PLAYER__PARTICIPATIONS:
        getParticipations().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case DartPackage.PLAYER__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case DartPackage.PLAYER__LEAGUE:
        return basicGetLeague() != null;
      case DartPackage.PLAYER__PARTICIPATIONS:
        return participations != null && !participations.isEmpty();
      case DartPackage.PLAYER__WIN_COUNT:
        return getWinCount() != WIN_COUNT_EDEFAULT;
      case DartPackage.PLAYER__BEST_THROW:
        return getBestThrow() != BEST_THROW_EDEFAULT;
      case DartPackage.PLAYER__GAME_COUNT:
        return getGameCount() != GAME_COUNT_EDEFAULT;
      case DartPackage.PLAYER__THROWS_AVG:
        return getThrowsAvg() != THROWS_AVG_EDEFAULT;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(')');
    return result.toString();
  }

} //PlayerImpl
