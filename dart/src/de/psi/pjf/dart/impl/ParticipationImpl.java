/**
 */
package de.psi.pjf.dart.impl;

import com.google.common.base.Objects;

import de.psi.pjf.dart.DartPackage;
import de.psi.pjf.dart.Game;
import de.psi.pjf.dart.GameType;
import de.psi.pjf.dart.Participation;
import de.psi.pjf.dart.Player;
import de.psi.pjf.dart.Throw;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.xtext.xbase.lib.Functions.Function2;

import org.eclipse.xtext.xbase.lib.IterableExtensions;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Participation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.psi.pjf.dart.impl.ParticipationImpl#getGame <em>Game</em>}</li>
 *   <li>{@link de.psi.pjf.dart.impl.ParticipationImpl#getThrows <em>Throws</em>}</li>
 *   <li>{@link de.psi.pjf.dart.impl.ParticipationImpl#getPlayer <em>Player</em>}</li>
 *   <li>{@link de.psi.pjf.dart.impl.ParticipationImpl#getToWin <em>To Win</em>}</li>
 *   <li>{@link de.psi.pjf.dart.impl.ParticipationImpl#getGameMax <em>Game Max</em>}</li>
 *   <li>{@link de.psi.pjf.dart.impl.ParticipationImpl#isWinner <em>Winner</em>}</li>
 *   <li>{@link de.psi.pjf.dart.impl.ParticipationImpl#getGameAvg <em>Game Avg</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ParticipationImpl extends MinimalEObjectImpl.Container implements Participation
{
  /**
   * The cached value of the '{@link #getThrows() <em>Throws</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getThrows()
   * @generated
   * @ordered
   */
  protected EList<Throw> throws_;

  /**
   * The cached value of the '{@link #getPlayer() <em>Player</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPlayer()
   * @generated
   * @ordered
   */
  protected Player player;

  /**
   * The default value of the '{@link #getToWin() <em>To Win</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getToWin()
   * @generated
   * @ordered
   */
  protected static final int TO_WIN_EDEFAULT = 0;

  /**
   * The default value of the '{@link #getGameMax() <em>Game Max</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGameMax()
   * @generated
   * @ordered
   */
  protected static final int GAME_MAX_EDEFAULT = 0;

  /**
   * The default value of the '{@link #isWinner() <em>Winner</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isWinner()
   * @generated
   * @ordered
   */
  protected static final boolean WINNER_EDEFAULT = false;

  /**
   * The default value of the '{@link #getGameAvg() <em>Game Avg</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGameAvg()
   * @generated
   * @ordered
   */
  protected static final double GAME_AVG_EDEFAULT = 0.0;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ParticipationImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return DartPackage.Literals.PARTICIPATION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Game getGame()
  {
    if (eContainerFeatureID() != DartPackage.PARTICIPATION__GAME) return null;
    return (Game)eContainer();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Game basicGetGame()
  {
    if (eContainerFeatureID() != DartPackage.PARTICIPATION__GAME) return null;
    return (Game)eInternalContainer();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetGame(Game newGame, NotificationChain msgs)
  {
    msgs = eBasicSetContainer((InternalEObject)newGame, DartPackage.PARTICIPATION__GAME, msgs);
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGame(Game newGame)
  {
    if (newGame != eInternalContainer() || (eContainerFeatureID() != DartPackage.PARTICIPATION__GAME && newGame != null))
    {
      if (EcoreUtil.isAncestor(this, newGame))
        throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
      NotificationChain msgs = null;
      if (eInternalContainer() != null)
        msgs = eBasicRemoveFromContainer(msgs);
      if (newGame != null)
        msgs = ((InternalEObject)newGame).eInverseAdd(this, DartPackage.GAME__PARTICIPATIONS, Game.class, msgs);
      msgs = basicSetGame(newGame, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DartPackage.PARTICIPATION__GAME, newGame, newGame));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Throw> getThrows()
  {
    if (throws_ == null)
    {
      throws_ = new EObjectContainmentWithInverseEList<Throw>(Throw.class, this, DartPackage.PARTICIPATION__THROWS, DartPackage.THROW__PARTICIPATION);
    }
    return throws_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Player getPlayer()
  {
    if (player != null && player.eIsProxy())
    {
      InternalEObject oldPlayer = (InternalEObject)player;
      player = (Player)eResolveProxy(oldPlayer);
      if (player != oldPlayer)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, DartPackage.PARTICIPATION__PLAYER, oldPlayer, player));
      }
    }
    return player;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Player basicGetPlayer()
  {
    return player;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetPlayer(Player newPlayer, NotificationChain msgs)
  {
    Player oldPlayer = player;
    player = newPlayer;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DartPackage.PARTICIPATION__PLAYER, oldPlayer, newPlayer);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPlayer(Player newPlayer)
  {
    if (newPlayer != player)
    {
      NotificationChain msgs = null;
      if (player != null)
        msgs = ((InternalEObject)player).eInverseRemove(this, DartPackage.PLAYER__PARTICIPATIONS, Player.class, msgs);
      if (newPlayer != null)
        msgs = ((InternalEObject)newPlayer).eInverseAdd(this, DartPackage.PLAYER__PARTICIPATIONS, Player.class, msgs);
      msgs = basicSetPlayer(newPlayer, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DartPackage.PARTICIPATION__PLAYER, newPlayer, newPlayer));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getToWin()
  {
    Integer _xblockexpression = null;
    {
      int _xifexpression = (int) 0;
      Participation _this = this;
      Game _game = _this.getGame();
      boolean _equals = Objects.equal(_game, null);
      if (_equals)
      {
        int _minus = (-100);
        _xifexpression = _minus;
      }
      else
      {
        Participation _this_1 = this;
        Game _game_1 = _this_1.getGame();
        GameType _gameType = _game_1.getGameType();
        int _value = _gameType.getValue();
        _xifexpression = _value;
      }
      final int s = _xifexpression;
      Participation _this_2 = this;
      EList<Throw> _throws = _this_2.getThrows();
      final Function2<Integer,Throw,Integer> _function = new Function2<Integer,Throw,Integer>()
      {
        public Integer apply(final Integer a, final Throw b)
        {
          Integer _xifexpression = null;
          int _total = b.getTotal();
          boolean _greaterThan = (_total > (a).intValue());
          if (_greaterThan)
          {
            _xifexpression = a;
          }
          else
          {
            int _total_1 = b.getTotal();
            int _minus = ((a).intValue() - _total_1);
            _xifexpression = Integer.valueOf(_minus);
          }
          return _xifexpression;
        }
      };
      Integer _fold = IterableExtensions.<Throw, Integer>fold(_throws, Integer.valueOf(s), _function);
      _xblockexpression = (_fold);
    }
    return (_xblockexpression).intValue();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getGameMax()
  {
    Participation _this = this;
    EList<Throw> _throws = _this.getThrows();
    final Function2<Integer,Throw,Integer> _function = new Function2<Integer,Throw,Integer>()
    {
      public Integer apply(final Integer a, final Throw b)
      {
        int _total = b.getTotal();
        int _max = Math.max((a).intValue(), _total);
        return Integer.valueOf(_max);
      }
    };
    Integer _fold = IterableExtensions.<Throw, Integer>fold(_throws, Integer.valueOf(0), _function);
    return (_fold).intValue();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isWinner()
  {
    Participation _this = this;
    int _toWin = _this.getToWin();
    boolean _equals = (_toWin == 0);
    return _equals;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public double getGameAvg()
  {
    double _xifexpression = (double) 0;
    Participation _this = this;
    EList<Throw> _throws = _this.getThrows();
    int _size = _throws.size();
    boolean _notEquals = (_size != 0);
    if (_notEquals)
    {
      Participation _this_1 = this;
      EList<Throw> _throws_1 = _this_1.getThrows();
      final Function2<Integer,Throw,Integer> _function = new Function2<Integer,Throw,Integer>()
      {
        public Integer apply(final Integer a, final Throw b)
        {
          int _total = b.getTotal();
          int _plus = ((a).intValue() + _total);
          return Integer.valueOf(_plus);
        }
      };
      Integer _fold = IterableExtensions.<Throw, Integer>fold(_throws_1, Integer.valueOf(0), _function);
      Participation _this_2 = this;
      EList<Throw> _throws_2 = _this_2.getThrows();
      int _size_1 = _throws_2.size();
      int _divide = ((_fold).intValue() / _size_1);
      _xifexpression = _divide;
    }
    else
    {
      _xifexpression = 0.0;
    }
    return _xifexpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case DartPackage.PARTICIPATION__GAME:
        if (eInternalContainer() != null)
          msgs = eBasicRemoveFromContainer(msgs);
        return basicSetGame((Game)otherEnd, msgs);
      case DartPackage.PARTICIPATION__THROWS:
        return ((InternalEList<InternalEObject>)(InternalEList<?>)getThrows()).basicAdd(otherEnd, msgs);
      case DartPackage.PARTICIPATION__PLAYER:
        if (player != null)
          msgs = ((InternalEObject)player).eInverseRemove(this, DartPackage.PLAYER__PARTICIPATIONS, Player.class, msgs);
        return basicSetPlayer((Player)otherEnd, msgs);
    }
    return super.eInverseAdd(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case DartPackage.PARTICIPATION__GAME:
        return basicSetGame(null, msgs);
      case DartPackage.PARTICIPATION__THROWS:
        return ((InternalEList<?>)getThrows()).basicRemove(otherEnd, msgs);
      case DartPackage.PARTICIPATION__PLAYER:
        return basicSetPlayer(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs)
  {
    switch (eContainerFeatureID())
    {
      case DartPackage.PARTICIPATION__GAME:
        return eInternalContainer().eInverseRemove(this, DartPackage.GAME__PARTICIPATIONS, Game.class, msgs);
    }
    return super.eBasicRemoveFromContainerFeature(msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case DartPackage.PARTICIPATION__GAME:
        if (resolve) return getGame();
        return basicGetGame();
      case DartPackage.PARTICIPATION__THROWS:
        return getThrows();
      case DartPackage.PARTICIPATION__PLAYER:
        if (resolve) return getPlayer();
        return basicGetPlayer();
      case DartPackage.PARTICIPATION__TO_WIN:
        return getToWin();
      case DartPackage.PARTICIPATION__GAME_MAX:
        return getGameMax();
      case DartPackage.PARTICIPATION__WINNER:
        return isWinner();
      case DartPackage.PARTICIPATION__GAME_AVG:
        return getGameAvg();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case DartPackage.PARTICIPATION__GAME:
        setGame((Game)newValue);
        return;
      case DartPackage.PARTICIPATION__THROWS:
        getThrows().clear();
        getThrows().addAll((Collection<? extends Throw>)newValue);
        return;
      case DartPackage.PARTICIPATION__PLAYER:
        setPlayer((Player)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case DartPackage.PARTICIPATION__GAME:
        setGame((Game)null);
        return;
      case DartPackage.PARTICIPATION__THROWS:
        getThrows().clear();
        return;
      case DartPackage.PARTICIPATION__PLAYER:
        setPlayer((Player)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case DartPackage.PARTICIPATION__GAME:
        return basicGetGame() != null;
      case DartPackage.PARTICIPATION__THROWS:
        return throws_ != null && !throws_.isEmpty();
      case DartPackage.PARTICIPATION__PLAYER:
        return player != null;
      case DartPackage.PARTICIPATION__TO_WIN:
        return getToWin() != TO_WIN_EDEFAULT;
      case DartPackage.PARTICIPATION__GAME_MAX:
        return getGameMax() != GAME_MAX_EDEFAULT;
      case DartPackage.PARTICIPATION__WINNER:
        return isWinner() != WINNER_EDEFAULT;
      case DartPackage.PARTICIPATION__GAME_AVG:
        return getGameAvg() != GAME_AVG_EDEFAULT;
    }
    return super.eIsSet(featureID);
  }

} //ParticipationImpl
