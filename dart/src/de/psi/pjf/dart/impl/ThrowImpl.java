/**
 */
package de.psi.pjf.dart.impl;

import com.google.common.base.Objects;

import de.psi.pjf.dart.DartPackage;
import de.psi.pjf.dart.Participation;
import de.psi.pjf.dart.Player;
import de.psi.pjf.dart.Throw;
import de.psi.pjf.dart.ThrowType;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Throw</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.psi.pjf.dart.impl.ThrowImpl#getParticipation <em>Participation</em>}</li>
 *   <li>{@link de.psi.pjf.dart.impl.ThrowImpl#getFirst <em>First</em>}</li>
 *   <li>{@link de.psi.pjf.dart.impl.ThrowImpl#getFirstType <em>First Type</em>}</li>
 *   <li>{@link de.psi.pjf.dart.impl.ThrowImpl#getSecond <em>Second</em>}</li>
 *   <li>{@link de.psi.pjf.dart.impl.ThrowImpl#getSecondType <em>Second Type</em>}</li>
 *   <li>{@link de.psi.pjf.dart.impl.ThrowImpl#getThird <em>Third</em>}</li>
 *   <li>{@link de.psi.pjf.dart.impl.ThrowImpl#getThirdType <em>Third Type</em>}</li>
 *   <li>{@link de.psi.pjf.dart.impl.ThrowImpl#getTotal <em>Total</em>}</li>
 *   <li>{@link de.psi.pjf.dart.impl.ThrowImpl#getDoubles <em>Doubles</em>}</li>
 *   <li>{@link de.psi.pjf.dart.impl.ThrowImpl#getTriples <em>Triples</em>}</li>
 *   <li>{@link de.psi.pjf.dart.impl.ThrowImpl#isBest <em>Best</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ThrowImpl extends MinimalEObjectImpl.Container implements Throw
{
  /**
   * The default value of the '{@link #getFirst() <em>First</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFirst()
   * @generated
   * @ordered
   */
  protected static final int FIRST_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getFirst() <em>First</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFirst()
   * @generated
   * @ordered
   */
  protected int first = FIRST_EDEFAULT;

  /**
   * The default value of the '{@link #getFirstType() <em>First Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFirstType()
   * @generated
   * @ordered
   */
  protected static final ThrowType FIRST_TYPE_EDEFAULT = ThrowType.NORMAL;

  /**
   * The cached value of the '{@link #getFirstType() <em>First Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFirstType()
   * @generated
   * @ordered
   */
  protected ThrowType firstType = FIRST_TYPE_EDEFAULT;

  /**
   * The default value of the '{@link #getSecond() <em>Second</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSecond()
   * @generated
   * @ordered
   */
  protected static final int SECOND_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getSecond() <em>Second</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSecond()
   * @generated
   * @ordered
   */
  protected int second = SECOND_EDEFAULT;

  /**
   * The default value of the '{@link #getSecondType() <em>Second Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSecondType()
   * @generated
   * @ordered
   */
  protected static final ThrowType SECOND_TYPE_EDEFAULT = ThrowType.NORMAL;

  /**
   * The cached value of the '{@link #getSecondType() <em>Second Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSecondType()
   * @generated
   * @ordered
   */
  protected ThrowType secondType = SECOND_TYPE_EDEFAULT;

  /**
   * The default value of the '{@link #getThird() <em>Third</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getThird()
   * @generated
   * @ordered
   */
  protected static final int THIRD_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getThird() <em>Third</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getThird()
   * @generated
   * @ordered
   */
  protected int third = THIRD_EDEFAULT;

  /**
   * The default value of the '{@link #getThirdType() <em>Third Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getThirdType()
   * @generated
   * @ordered
   */
  protected static final ThrowType THIRD_TYPE_EDEFAULT = ThrowType.NORMAL;

  /**
   * The cached value of the '{@link #getThirdType() <em>Third Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getThirdType()
   * @generated
   * @ordered
   */
  protected ThrowType thirdType = THIRD_TYPE_EDEFAULT;

  /**
   * The default value of the '{@link #getTotal() <em>Total</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTotal()
   * @generated
   * @ordered
   */
  protected static final int TOTAL_EDEFAULT = 0;

  /**
   * The default value of the '{@link #getDoubles() <em>Doubles</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDoubles()
   * @generated
   * @ordered
   */
  protected static final int DOUBLES_EDEFAULT = 0;

  /**
   * The default value of the '{@link #getTriples() <em>Triples</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTriples()
   * @generated
   * @ordered
   */
  protected static final int TRIPLES_EDEFAULT = 0;

  /**
   * The default value of the '{@link #isBest() <em>Best</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isBest()
   * @generated
   * @ordered
   */
  protected static final boolean BEST_EDEFAULT = false;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ThrowImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return DartPackage.Literals.THROW;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Participation getParticipation()
  {
    if (eContainerFeatureID() != DartPackage.THROW__PARTICIPATION) return null;
    return (Participation)eContainer();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Participation basicGetParticipation()
  {
    if (eContainerFeatureID() != DartPackage.THROW__PARTICIPATION) return null;
    return (Participation)eInternalContainer();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetParticipation(Participation newParticipation, NotificationChain msgs)
  {
    msgs = eBasicSetContainer((InternalEObject)newParticipation, DartPackage.THROW__PARTICIPATION, msgs);
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setParticipation(Participation newParticipation)
  {
    if (newParticipation != eInternalContainer() || (eContainerFeatureID() != DartPackage.THROW__PARTICIPATION && newParticipation != null))
    {
      if (EcoreUtil.isAncestor(this, newParticipation))
        throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
      NotificationChain msgs = null;
      if (eInternalContainer() != null)
        msgs = eBasicRemoveFromContainer(msgs);
      if (newParticipation != null)
        msgs = ((InternalEObject)newParticipation).eInverseAdd(this, DartPackage.PARTICIPATION__THROWS, Participation.class, msgs);
      msgs = basicSetParticipation(newParticipation, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DartPackage.THROW__PARTICIPATION, newParticipation, newParticipation));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getFirst()
  {
    return first;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFirst(int newFirst)
  {
    int oldFirst = first;
    first = newFirst;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DartPackage.THROW__FIRST, oldFirst, first));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ThrowType getFirstType()
  {
    return firstType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFirstType(ThrowType newFirstType)
  {
    ThrowType oldFirstType = firstType;
    firstType = newFirstType == null ? FIRST_TYPE_EDEFAULT : newFirstType;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DartPackage.THROW__FIRST_TYPE, oldFirstType, firstType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getSecond()
  {
    return second;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSecond(int newSecond)
  {
    int oldSecond = second;
    second = newSecond;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DartPackage.THROW__SECOND, oldSecond, second));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ThrowType getSecondType()
  {
    return secondType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSecondType(ThrowType newSecondType)
  {
    ThrowType oldSecondType = secondType;
    secondType = newSecondType == null ? SECOND_TYPE_EDEFAULT : newSecondType;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DartPackage.THROW__SECOND_TYPE, oldSecondType, secondType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getThird()
  {
    return third;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setThird(int newThird)
  {
    int oldThird = third;
    third = newThird;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DartPackage.THROW__THIRD, oldThird, third));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ThrowType getThirdType()
  {
    return thirdType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setThirdType(ThrowType newThirdType)
  {
    ThrowType oldThirdType = thirdType;
    thirdType = newThirdType == null ? THIRD_TYPE_EDEFAULT : newThirdType;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DartPackage.THROW__THIRD_TYPE, oldThirdType, thirdType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getTotal()
  {
    Throw _this = this;
    int _first = _this.getFirst();
    Throw _this_1 = this;
    ThrowType _firstType = _this_1.getFirstType();
    int _value = _firstType.getValue();
    int _multiply = (_first * _value);
    Throw _this_2 = this;
    int _second = _this_2.getSecond();
    Throw _this_3 = this;
    ThrowType _secondType = _this_3.getSecondType();
    int _value_1 = _secondType.getValue();
    int _multiply_1 = (_second * _value_1);
    int _plus = (_multiply + _multiply_1);
    Throw _this_4 = this;
    int _third = _this_4.getThird();
    Throw _this_5 = this;
    ThrowType _thirdType = _this_5.getThirdType();
    int _value_2 = _thirdType.getValue();
    int _multiply_2 = (_third * _value_2);
    int _plus_1 = (_plus + _multiply_2);
    return _plus_1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getDoubles()
  {
    int _xblockexpression = (int) 0;
    {
      int _xifexpression = (int) 0;
      Throw _this = this;
      ThrowType _firstType = _this.getFirstType();
      boolean _equals = Objects.equal(_firstType, ThrowType.DOUBLE);
      if (_equals)
      {
        _xifexpression = 1;
      }
      else
      {
        _xifexpression = 0;
      }
      int doubles = _xifexpression;
      int _xifexpression_1 = (int) 0;
      Throw _this_1 = this;
      ThrowType _secondType = _this_1.getSecondType();
      boolean _equals_1 = Objects.equal(_secondType, ThrowType.DOUBLE);
      if (_equals_1)
      {
        int _plus = (doubles + 1);
        _xifexpression_1 = _plus;
      }
      else
      {
        _xifexpression_1 = doubles;
      }
      doubles = _xifexpression_1;
      int _xifexpression_2 = (int) 0;
      Throw _this_2 = this;
      ThrowType _thirdType = _this_2.getThirdType();
      boolean _equals_2 = Objects.equal(_thirdType, ThrowType.DOUBLE);
      if (_equals_2)
      {
        int _plus_1 = (doubles + 1);
        _xifexpression_2 = _plus_1;
      }
      else
      {
        _xifexpression_2 = doubles;
      }
      doubles = _xifexpression_2;
      _xblockexpression = (doubles);
    }
    return _xblockexpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getTriples()
  {
    int _xblockexpression = (int) 0;
    {
      int _xifexpression = (int) 0;
      Throw _this = this;
      ThrowType _firstType = _this.getFirstType();
      boolean _equals = Objects.equal(_firstType, ThrowType.TRIPLE);
      if (_equals)
      {
        _xifexpression = 1;
      }
      else
      {
        _xifexpression = 0;
      }
      int triples = _xifexpression;
      int _xifexpression_1 = (int) 0;
      Throw _this_1 = this;
      ThrowType _secondType = _this_1.getSecondType();
      boolean _equals_1 = Objects.equal(_secondType, ThrowType.TRIPLE);
      if (_equals_1)
      {
        int _plus = (triples + 1);
        _xifexpression_1 = _plus;
      }
      else
      {
        _xifexpression_1 = triples;
      }
      triples = _xifexpression_1;
      int _xifexpression_2 = (int) 0;
      Throw _this_2 = this;
      ThrowType _thirdType = _this_2.getThirdType();
      boolean _equals_2 = Objects.equal(_thirdType, ThrowType.TRIPLE);
      if (_equals_2)
      {
        int _plus_1 = (triples + 1);
        _xifexpression_2 = _plus_1;
      }
      else
      {
        _xifexpression_2 = triples;
      }
      triples = _xifexpression_2;
      _xblockexpression = (triples);
    }
    return _xblockexpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isBest()
  {
    boolean _xblockexpression = false;
    {
      Throw _this = this;
      Participation _participation = _this.getParticipation();
      Player _player = null;
      if (_participation!=null)
      {
        _player=_participation.getPlayer();
      }
      final Player player = _player;
      boolean _xifexpression = false;
      boolean _equals = Objects.equal(player, null);
      if (_equals)
      {
        _xifexpression = false;
      }
      else
      {
        Throw _this_1 = this;
        int _total = _this_1.getTotal();
        int _bestThrow = player.getBestThrow();
        boolean _equals_1 = (_total == _bestThrow);
        _xifexpression = _equals_1;
      }
      _xblockexpression = (_xifexpression);
    }
    return _xblockexpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case DartPackage.THROW__PARTICIPATION:
        if (eInternalContainer() != null)
          msgs = eBasicRemoveFromContainer(msgs);
        return basicSetParticipation((Participation)otherEnd, msgs);
    }
    return super.eInverseAdd(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case DartPackage.THROW__PARTICIPATION:
        return basicSetParticipation(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs)
  {
    switch (eContainerFeatureID())
    {
      case DartPackage.THROW__PARTICIPATION:
        return eInternalContainer().eInverseRemove(this, DartPackage.PARTICIPATION__THROWS, Participation.class, msgs);
    }
    return super.eBasicRemoveFromContainerFeature(msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case DartPackage.THROW__PARTICIPATION:
        if (resolve) return getParticipation();
        return basicGetParticipation();
      case DartPackage.THROW__FIRST:
        return getFirst();
      case DartPackage.THROW__FIRST_TYPE:
        return getFirstType();
      case DartPackage.THROW__SECOND:
        return getSecond();
      case DartPackage.THROW__SECOND_TYPE:
        return getSecondType();
      case DartPackage.THROW__THIRD:
        return getThird();
      case DartPackage.THROW__THIRD_TYPE:
        return getThirdType();
      case DartPackage.THROW__TOTAL:
        return getTotal();
      case DartPackage.THROW__DOUBLES:
        return getDoubles();
      case DartPackage.THROW__TRIPLES:
        return getTriples();
      case DartPackage.THROW__BEST:
        return isBest();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case DartPackage.THROW__PARTICIPATION:
        setParticipation((Participation)newValue);
        return;
      case DartPackage.THROW__FIRST:
        setFirst((Integer)newValue);
        return;
      case DartPackage.THROW__FIRST_TYPE:
        setFirstType((ThrowType)newValue);
        return;
      case DartPackage.THROW__SECOND:
        setSecond((Integer)newValue);
        return;
      case DartPackage.THROW__SECOND_TYPE:
        setSecondType((ThrowType)newValue);
        return;
      case DartPackage.THROW__THIRD:
        setThird((Integer)newValue);
        return;
      case DartPackage.THROW__THIRD_TYPE:
        setThirdType((ThrowType)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case DartPackage.THROW__PARTICIPATION:
        setParticipation((Participation)null);
        return;
      case DartPackage.THROW__FIRST:
        setFirst(FIRST_EDEFAULT);
        return;
      case DartPackage.THROW__FIRST_TYPE:
        setFirstType(FIRST_TYPE_EDEFAULT);
        return;
      case DartPackage.THROW__SECOND:
        setSecond(SECOND_EDEFAULT);
        return;
      case DartPackage.THROW__SECOND_TYPE:
        setSecondType(SECOND_TYPE_EDEFAULT);
        return;
      case DartPackage.THROW__THIRD:
        setThird(THIRD_EDEFAULT);
        return;
      case DartPackage.THROW__THIRD_TYPE:
        setThirdType(THIRD_TYPE_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case DartPackage.THROW__PARTICIPATION:
        return basicGetParticipation() != null;
      case DartPackage.THROW__FIRST:
        return first != FIRST_EDEFAULT;
      case DartPackage.THROW__FIRST_TYPE:
        return firstType != FIRST_TYPE_EDEFAULT;
      case DartPackage.THROW__SECOND:
        return second != SECOND_EDEFAULT;
      case DartPackage.THROW__SECOND_TYPE:
        return secondType != SECOND_TYPE_EDEFAULT;
      case DartPackage.THROW__THIRD:
        return third != THIRD_EDEFAULT;
      case DartPackage.THROW__THIRD_TYPE:
        return thirdType != THIRD_TYPE_EDEFAULT;
      case DartPackage.THROW__TOTAL:
        return getTotal() != TOTAL_EDEFAULT;
      case DartPackage.THROW__DOUBLES:
        return getDoubles() != DOUBLES_EDEFAULT;
      case DartPackage.THROW__TRIPLES:
        return getTriples() != TRIPLES_EDEFAULT;
      case DartPackage.THROW__BEST:
        return isBest() != BEST_EDEFAULT;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (first: ");
    result.append(first);
    result.append(", firstType: ");
    result.append(firstType);
    result.append(", second: ");
    result.append(second);
    result.append(", secondType: ");
    result.append(secondType);
    result.append(", third: ");
    result.append(third);
    result.append(", thirdType: ");
    result.append(thirdType);
    result.append(')');
    return result.toString();
  }

} //ThrowImpl
