/**
 */
package de.psi.pjf.dart.impl;

import java.util.Collection;
import java.util.Date;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

import de.psi.pjf.dart.DartPackage;
import de.psi.pjf.dart.Game;
import de.psi.pjf.dart.GameType;
import de.psi.pjf.dart.League;
import de.psi.pjf.dart.Participation;
import de.psi.pjf.dart.Player;
import java.lang.Iterable;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Game</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.psi.pjf.dart.impl.GameImpl#getLeague <em>League</em>}</li>
 *   <li>{@link de.psi.pjf.dart.impl.GameImpl#getParticipations <em>Participations</em>}</li>
 *   <li>{@link de.psi.pjf.dart.impl.GameImpl#getGameType <em>Game Type</em>}</li>
 *   <li>{@link de.psi.pjf.dart.impl.GameImpl#getWinners <em>Winners</em>}</li>
 *   <li>{@link de.psi.pjf.dart.impl.GameImpl#getCreated <em>Created</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class GameImpl extends MinimalEObjectImpl.Container implements Game
{
    /**
   * The cached value of the '{@link #getParticipations() <em>Participations</em>}' containment reference list.
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @see #getParticipations()
   * @generated
   * @ordered
   */
    protected EList<Participation> participations;

    /**
   * The default value of the '{@link #getGameType() <em>Game Type</em>}' attribute.
   * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
   * @see #getGameType()
   * @generated
   * @ordered
   */
    protected static final GameType GAME_TYPE_EDEFAULT = GameType.G301;

    /**
   * The cached value of the '{@link #getGameType() <em>Game Type</em>}' attribute.
   * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
   * @see #getGameType()
   * @generated
   * @ordered
   */
    protected GameType gameType = GAME_TYPE_EDEFAULT;

    /**
   * The default value of the '{@link #getCreated() <em>Created</em>}' attribute.
   * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
   * @see #getCreated()
   * @generated
   * @ordered
   */
    protected static final Date CREATED_EDEFAULT = null;

    /**
   * The cached value of the '{@link #getCreated() <em>Created</em>}' attribute.
   * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
   * @see #getCreated()
   * @generated
   * @ordered
   */
    protected Date created = CREATED_EDEFAULT;

    /**
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    protected GameImpl()
    {
    super();
  }

    /**
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    @Override
    protected EClass eStaticClass()
    {
    return DartPackage.Literals.GAME;
  }

    /**
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    @Override
    public League getLeague()
    {
    if (eContainerFeatureID() != DartPackage.GAME__LEAGUE) return null;
    return (League)eContainer();
  }

    /**
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    public League basicGetLeague()
    {
    if (eContainerFeatureID() != DartPackage.GAME__LEAGUE) return null;
    return (League)eInternalContainer();
  }

    /**
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    public NotificationChain basicSetLeague( League newLeague, NotificationChain msgs )
    {
    msgs = eBasicSetContainer((InternalEObject)newLeague, DartPackage.GAME__LEAGUE, msgs);
    return msgs;
  }

    /**
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    @Override
    public void setLeague( League newLeague )
    {
    if (newLeague != eInternalContainer() || (eContainerFeatureID() != DartPackage.GAME__LEAGUE && newLeague != null))
    {
      if (EcoreUtil.isAncestor(this, newLeague))
        throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
      NotificationChain msgs = null;
      if (eInternalContainer() != null)
        msgs = eBasicRemoveFromContainer(msgs);
      if (newLeague != null)
        msgs = ((InternalEObject)newLeague).eInverseAdd(this, DartPackage.LEAGUE__GAMES, League.class, msgs);
      msgs = basicSetLeague(newLeague, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DartPackage.GAME__LEAGUE, newLeague, newLeague));
  }

    /**
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    @Override
    public EList<Participation> getParticipations()
    {
    if (participations == null)
    {
      participations = new EObjectContainmentWithInverseEList<Participation>(Participation.class, this, DartPackage.GAME__PARTICIPATIONS, DartPackage.PARTICIPATION__GAME);
    }
    return participations;
  }

    /**
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    @Override
    public GameType getGameType()
    {
    return gameType;
  }

    /**
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    @Override
    public void setGameType( GameType newGameType )
    {
    GameType oldGameType = gameType;
    gameType = newGameType == null ? GAME_TYPE_EDEFAULT : newGameType;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DartPackage.GAME__GAME_TYPE, oldGameType, gameType));
  }

    /**
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    @Override
    public EList<Player> getWinners()
    {
    Game _this = this;
    EList<Participation> _participations = _this.getParticipations();
    final Function1<Participation,Boolean> _function = new Function1<Participation,Boolean>()
    {
      public Boolean apply(final Participation it)
      {
        boolean _isWinner = it.isWinner();
        return Boolean.valueOf(_isWinner);
      }
    };
    Iterable<Participation> _filter = IterableExtensions.<Participation>filter(_participations, _function);
    final Function1<Participation,Player> _function_1 = new Function1<Participation,Player>()
    {
      public Player apply(final Participation it)
      {
        Player _player = it.getPlayer();
        return _player;
      }
    };
    Iterable<Player> _map = IterableExtensions.<Participation, Player>map(_filter, _function_1);
    EList<Player> _eList = ECollections.<Player>toEList(_map);
    return _eList;
  }

    /**
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    @Override
    public Date getCreated()
    {
    return created;
  }

    /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCreated(Date newCreated)
  {
    Date oldCreated = created;
    created = newCreated;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DartPackage.GAME__CREATED, oldCreated, created));
  }

    /**
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    @SuppressWarnings( "unchecked" )
    @Override
    public NotificationChain eInverseAdd( InternalEObject otherEnd, int featureID, NotificationChain msgs )
    {
    switch (featureID)
    {
      case DartPackage.GAME__LEAGUE:
        if (eInternalContainer() != null)
          msgs = eBasicRemoveFromContainer(msgs);
        return basicSetLeague((League)otherEnd, msgs);
      case DartPackage.GAME__PARTICIPATIONS:
        return ((InternalEList<InternalEObject>)(InternalEList<?>)getParticipations()).basicAdd(otherEnd, msgs);
    }
    return super.eInverseAdd(otherEnd, featureID, msgs);
  }

    /**
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    @Override
    public NotificationChain eInverseRemove( InternalEObject otherEnd, int featureID, NotificationChain msgs )
    {
    switch (featureID)
    {
      case DartPackage.GAME__LEAGUE:
        return basicSetLeague(null, msgs);
      case DartPackage.GAME__PARTICIPATIONS:
        return ((InternalEList<?>)getParticipations()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

    /**
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    @Override
    public NotificationChain eBasicRemoveFromContainerFeature( NotificationChain msgs )
    {
    switch (eContainerFeatureID())
    {
      case DartPackage.GAME__LEAGUE:
        return eInternalContainer().eInverseRemove(this, DartPackage.LEAGUE__GAMES, League.class, msgs);
    }
    return super.eBasicRemoveFromContainerFeature(msgs);
  }

    /**
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    @Override
    public Object eGet( int featureID, boolean resolve, boolean coreType )
    {
    switch (featureID)
    {
      case DartPackage.GAME__LEAGUE:
        if (resolve) return getLeague();
        return basicGetLeague();
      case DartPackage.GAME__PARTICIPATIONS:
        return getParticipations();
      case DartPackage.GAME__GAME_TYPE:
        return getGameType();
      case DartPackage.GAME__WINNERS:
        return getWinners();
      case DartPackage.GAME__CREATED:
        return getCreated();
    }
    return super.eGet(featureID, resolve, coreType);
  }

    /**
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    @SuppressWarnings( "unchecked" )
    @Override
    public void eSet( int featureID, Object newValue )
    {
    switch (featureID)
    {
      case DartPackage.GAME__LEAGUE:
        setLeague((League)newValue);
        return;
      case DartPackage.GAME__PARTICIPATIONS:
        getParticipations().clear();
        getParticipations().addAll((Collection<? extends Participation>)newValue);
        return;
      case DartPackage.GAME__GAME_TYPE:
        setGameType((GameType)newValue);
        return;
      case DartPackage.GAME__CREATED:
        setCreated((Date)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

    /**
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    @Override
    public void eUnset( int featureID )
    {
    switch (featureID)
    {
      case DartPackage.GAME__LEAGUE:
        setLeague((League)null);
        return;
      case DartPackage.GAME__PARTICIPATIONS:
        getParticipations().clear();
        return;
      case DartPackage.GAME__GAME_TYPE:
        setGameType(GAME_TYPE_EDEFAULT);
        return;
      case DartPackage.GAME__CREATED:
        setCreated(CREATED_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

    /**
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    @Override
    public boolean eIsSet( int featureID )
    {
    switch (featureID)
    {
      case DartPackage.GAME__LEAGUE:
        return basicGetLeague() != null;
      case DartPackage.GAME__PARTICIPATIONS:
        return participations != null && !participations.isEmpty();
      case DartPackage.GAME__GAME_TYPE:
        return gameType != GAME_TYPE_EDEFAULT;
      case DartPackage.GAME__WINNERS:
        return !getWinners().isEmpty();
      case DartPackage.GAME__CREATED:
        return CREATED_EDEFAULT == null ? created != null : !CREATED_EDEFAULT.equals(created);
    }
    return super.eIsSet(featureID);
  }

    /**
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    @Override
    public String toString()
    {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (gameType: ");
    result.append(gameType);
    result.append(", created: ");
    result.append(created);
    result.append(')');
    return result.toString();
  }

} // GameImpl
