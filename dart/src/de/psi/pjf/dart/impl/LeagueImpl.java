/**
 */
package de.psi.pjf.dart.impl;

import de.psi.pjf.dart.DartPackage;
import de.psi.pjf.dart.Game;
import de.psi.pjf.dart.League;
import de.psi.pjf.dart.Player;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>League</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.psi.pjf.dart.impl.LeagueImpl#getName <em>Name</em>}</li>
 *   <li>{@link de.psi.pjf.dart.impl.LeagueImpl#getPlayers <em>Players</em>}</li>
 *   <li>{@link de.psi.pjf.dart.impl.LeagueImpl#getGames <em>Games</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class LeagueImpl extends MinimalEObjectImpl.Container implements League
{
  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The cached value of the '{@link #getPlayers() <em>Players</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPlayers()
   * @generated
   * @ordered
   */
  protected EList<Player> players;

  /**
   * The cached value of the '{@link #getGames() <em>Games</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGames()
   * @generated
   * @ordered
   */
  protected EList<Game> games;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected LeagueImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return DartPackage.Literals.LEAGUE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DartPackage.LEAGUE__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Player> getPlayers()
  {
    if (players == null)
    {
      players = new EObjectContainmentWithInverseEList<Player>(Player.class, this, DartPackage.LEAGUE__PLAYERS, DartPackage.PLAYER__LEAGUE);
    }
    return players;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Game> getGames()
  {
    if (games == null)
    {
      games = new EObjectContainmentWithInverseEList<Game>(Game.class, this, DartPackage.LEAGUE__GAMES, DartPackage.GAME__LEAGUE);
    }
    return games;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case DartPackage.LEAGUE__PLAYERS:
        return ((InternalEList<InternalEObject>)(InternalEList<?>)getPlayers()).basicAdd(otherEnd, msgs);
      case DartPackage.LEAGUE__GAMES:
        return ((InternalEList<InternalEObject>)(InternalEList<?>)getGames()).basicAdd(otherEnd, msgs);
    }
    return super.eInverseAdd(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case DartPackage.LEAGUE__PLAYERS:
        return ((InternalEList<?>)getPlayers()).basicRemove(otherEnd, msgs);
      case DartPackage.LEAGUE__GAMES:
        return ((InternalEList<?>)getGames()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case DartPackage.LEAGUE__NAME:
        return getName();
      case DartPackage.LEAGUE__PLAYERS:
        return getPlayers();
      case DartPackage.LEAGUE__GAMES:
        return getGames();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case DartPackage.LEAGUE__NAME:
        setName((String)newValue);
        return;
      case DartPackage.LEAGUE__PLAYERS:
        getPlayers().clear();
        getPlayers().addAll((Collection<? extends Player>)newValue);
        return;
      case DartPackage.LEAGUE__GAMES:
        getGames().clear();
        getGames().addAll((Collection<? extends Game>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case DartPackage.LEAGUE__NAME:
        setName(NAME_EDEFAULT);
        return;
      case DartPackage.LEAGUE__PLAYERS:
        getPlayers().clear();
        return;
      case DartPackage.LEAGUE__GAMES:
        getGames().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case DartPackage.LEAGUE__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case DartPackage.LEAGUE__PLAYERS:
        return players != null && !players.isEmpty();
      case DartPackage.LEAGUE__GAMES:
        return games != null && !games.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(')');
    return result.toString();
  }

} //LeagueImpl
