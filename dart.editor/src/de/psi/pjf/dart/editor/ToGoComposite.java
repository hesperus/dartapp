package de.psi.pjf.dart.editor;

import java.util.Collection;
import java.util.Set;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.Collections2;
import com.google.common.collect.Sets;

import de.psi.pjf.dart.Game;
import de.psi.pjf.dart.Participation;

public class ToGoComposite {

	private Game game;
	private Label toGo;
	private Participation participation;
	private Composite aParent;

	public ToGoComposite(Participation aParticipation) {

		this.participation = aParticipation;
		aParticipation.eAdapters().add(extracted());
	}

	private EContentAdapter extracted() {
		return new EContentAdapter() {
			@Override
			public void notifyChanged(Notification notification) {
				super.notifyChanged(notification);
				Display.getCurrent().asyncExec(new Runnable() {
					@Override
					public void run() {
						if (aParent != null && !aParent.isDisposed())
							updateToGo();
					}
				});
			}
		};
	}

	public void init(Composite aParent) {
		aParent = aParent;
		Composite composite = new Composite(aParent, SWT.NONE);
		RowLayout layout = new RowLayout(SWT.HORIZONTAL);
		layout.justify = true;
		composite.setLayout(layout);

		toGo = new Label(composite, SWT.NONE);
		FontData fd = new FontData();
		fd.setHeight(20);
		fd.setStyle(SWT.BOLD);
		toGo.setFont(new Font(composite.getDisplay(), fd));

		updateToGo();
	}

	private Set<Participation> findBestThrow() {
		Set<Participation> maxSet = null;
		int gameMax = -1;
		for (Participation part : game.getParticipations()) {
			if (part.getGameMax() > gameMax) {
				maxSet = Sets.newHashSet();
				maxSet.add(part);
				gameMax = part.getGameMax();
			} else if (part.getGameMax() == gameMax) {
				maxSet.add(part);
			}
		}
		return maxSet;
	}

	private void updateToGo() {
		toGo.setText(String.valueOf(participation.getToWin()));
	}

}
