package de.psi.pjf.dart.editor;

import org.eclipse.emf.ecp.core.ECPProject;
import org.eclipse.emf.ecp.ui.util.ECPModelElementOpener;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import de.psi.pjf.dart.Game;

public class GameEditorOpener implements ECPModelElementOpener {

	@Override
	public void openModelElement(Object element, ECPProject ecpProject) {
		GameInput input = new GameInput((Game) element);
		try {
			PlatformUI
					.getWorkbench()
					.getActiveWorkbenchWindow()
					.getActivePage()
					.openEditor(
							input,
							"de.psi.pjf.dart.editor.GameEditor",
							true,
							IWorkbenchPage.MATCH_INPUT
									| IWorkbenchPage.MATCH_ID);

		} catch (PartInitException e) {
		}
	}
}
