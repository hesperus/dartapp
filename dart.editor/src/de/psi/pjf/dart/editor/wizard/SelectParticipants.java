package de.psi.pjf.dart.editor.wizard;

import java.util.ArrayList;

import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.List;

import de.psi.pjf.dart.Player;

public class SelectParticipants extends WizardPage
{
    private Composite container;
    private List list;
    private EList< Player > players;

    public SelectParticipants( EList< Player > eList )
    {
        super( "Second Page" );
        players = eList;
        setTitle( "Second Page" );
        setDescription( "Now this is the second page" );
    }

    @Override
    public void createControl( Composite parent )
    {
        container = new Composite( parent, SWT.NONE );
        GridLayout layout = new GridLayout();
        container.setLayout( layout );
        list = new List( container, SWT.MULTI | SWT.V_SCROLL | SWT.BORDER );
        list.addSelectionListener( new SelectionAdapter()
        {

            @Override
            public void widgetSelected( SelectionEvent e )
            {
                setPageComplete( list.getSelectionCount() > 1 );
            }
        } );
        for( Player player : players )
        {
            list.add( player.getName() );
        }

        // Required to avoid an error in the system
        setControl( container );
        setPageComplete( false );
    }

    java.util.List< Player > getPlayers()
    {
        int[] selectionIndices = list.getSelectionIndices();

        java.util.List< Player > players = new ArrayList< Player >();
        for( int i : selectionIndices )
        {
            players.add( this.players.get( i ) );
        }

        return players;
    }
}
