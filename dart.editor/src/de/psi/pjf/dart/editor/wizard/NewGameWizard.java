package de.psi.pjf.dart.editor.wizard;

import java.util.Date;
import java.util.List;

import org.eclipse.jface.wizard.Wizard;

import de.psi.pjf.dart.Game;
import de.psi.pjf.dart.League;
import de.psi.pjf.dart.Participation;
import de.psi.pjf.dart.Player;
import de.psi.pjf.dart.impl.DartFactoryImpl;

public class NewGameWizard extends Wizard {

	protected SelectGameType one;
	protected SelectParticipants two;
	private League league;

	public NewGameWizard(League league) {
		super();
		this.league = league;
		setNeedsProgressMonitor(true);
	}

	@Override
	public void addPages() {
		one = new SelectGameType();
		two = new SelectParticipants(league.getPlayers());
		addPage(one);
		addPage(two);
	}

	@Override
	public boolean performFinish() {
		Game game = DartFactoryImpl.eINSTANCE.createGame();
		game.setCreated(new Date());
		game.setGameType(one.getSelected());
		league.getGames().add(game);
		
		List<Player> players = two.getPlayers();
		for (Player player : players) {
			Participation participation = DartFactoryImpl.eINSTANCE.createParticipation();
			player.getParticipations().add(participation);
			game.getParticipations().add(participation);
			participation.setGame(game);
			participation.setPlayer(player);
		}
		return true;
	}
}
