package de.psi.pjf.dart.editor.wizard;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.commands.IHandlerListener;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import de.psi.pjf.dart.League;

public class WizardCommandHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		TreeSelection selection = (TreeSelection) HandlerUtil
				.getActiveMenuSelection(event);
		League firstElement = (League) selection.getFirstElement();

		WizardDialog wizard = new WizardDialog(
				HandlerUtil.getActiveShell(event), new NewGameWizard(firstElement));
		wizard.open();
		return null;
	}

}
