package de.psi.pjf.dart.editor.wizard;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import de.psi.pjf.dart.GameType;

public class SelectGameType extends WizardPage {
	private Composite container;
	private List<Button> buttons;

	public SelectGameType() {
		super("First Page");
		setTitle("First Page");
		setDescription("Fake Wizard. First page");
	}

	@Override
	public void createControl(Composite parent) {
		container = new Composite(parent, SWT.NONE);

		GridLayout layout = new GridLayout();
		container.setLayout(layout);

		buttons = new ArrayList<Button>();

		for (GameType type : GameType.VALUES) {
			Button button = new Button(container, SWT.RADIO);
			button.setText(type.getName());
			buttons.add(button);
		}
		// // Required to avoid an error in the system
		setControl(container);
		setPageComplete(true);

	}

	GameType getSelected() {
		for (Button button : buttons) {
			if (button.getSelection())
				return (GameType) button.getData();
		}
		throw new IllegalStateException();
	}
}
