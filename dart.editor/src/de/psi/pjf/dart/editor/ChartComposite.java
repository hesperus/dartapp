package de.psi.pjf.dart.editor;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.swtchart.Chart;
import org.swtchart.IAxis;
import org.swtchart.IAxisSet;
import org.swtchart.ILineSeries;
import org.swtchart.ISeries;
import org.swtchart.ISeriesSet;
import org.swtchart.LineStyle;
import org.swtchart.Range;
import org.swtchart.ILineSeries.PlotSymbolType;
import org.swtchart.ISeries.SeriesType;

import de.psi.pjf.dart.Game;
import de.psi.pjf.dart.Participation;
import de.psi.pjf.dart.Throw;

public class ChartComposite {
	public ChartComposite(Game aGame) {
		input = aGame;
		aGame.eAdapters().add(new EContentAdapter() {
			@Override
			public void notifyChanged(Notification notification) {
				super.notifyChanged(notification);
				if (chart != null) {
					Display.getCurrent().asyncExec(new Runnable() {
						@Override
						public void run() {
							try {
								if (!aParent.isDisposed()) {
									updateChart();
									chart.redraw();
								}
							} catch (Exception e) {
							}
						}
					});
				}
			}
		});
	}

	private Game input;
	private Chart chart;
	private Composite aParent;

	public void init(Composite parent) {
		this.aParent = parent;
		chart = new Chart(parent, SWT.NONE);
		chart.getTitle().setText("Game");
		chart.getAxisSet().getXAxis(0).getTitle().setText("Throws");
		chart.getAxisSet().getYAxis(0).getTitle().setText("Points");
		updateChart();
	}

	private void updateChart() {
		IAxisSet axisSet = chart.getAxisSet();
		IAxis yAxis = axisSet.getYAxis(0);

		yAxis.setRange(new Range(0, input.getGameType().getValue()));

		for (Participation part : input.getParticipations()) {
			createLineSeries(chart.getSeriesSet(), part);
		}
		IAxis xAxis = axisSet.getXAxis(0);
		xAxis.adjustRange();
	}

	private void createLineSeries(ISeriesSet iSeriesSet, Participation part) {
		ISeries series = iSeriesSet.getSeries(part.getPlayer().getName());
		if (series == null) {
			series = iSeriesSet.createSeries(SeriesType.LINE, part.getPlayer()
					.getName());
			ILineSeries iLineSeries = (ILineSeries) series;
			iLineSeries.setLineWidth(3);
			int hash = part.getPlayer().getName().hashCode();
			iLineSeries.setLineColor(new Color(Display.getCurrent(), new RGB(
					Math.abs((hash * (hash % 10))) % 256, Math
							.abs((hash * (hash % 7))) % 256, Math
							.abs((hash * (hash % 3))) % 256)));
		}
		fillSeries(series, part.getThrows());

		ISeries series1 = iSeriesSet.getSeries(part.getPlayer().getName()
				+ "avg");
		if (series1 == null) {
			series1 = iSeriesSet.createSeries(SeriesType.LINE, part.getPlayer()
					.getName()+"avg");
			ILineSeries iLineSeries = (ILineSeries) series1;
			iLineSeries.setLineWidth(1);
			iLineSeries.setSymbolType(PlotSymbolType.NONE);
			int hash = part.getPlayer().getName().hashCode();
			iLineSeries.setLineColor(new Color(Display.getCurrent(), new RGB(
					Math.abs((hash * (hash % 10))) % 256, Math
							.abs((hash * (hash % 7))) % 256, Math
							.abs((hash * (hash % 3))) % 256)));
			series.setVisibleInLegend(false);
		}
		fillSeriesAvg(series1, part.getGameAvg(), part.getThrows().size());
	}

	private void fillSeriesAvg(ISeries series, double avg, int throwsSize) {
		double[] xValues = new double[2];
		double[] yValues = new double[2];

		xValues[0] = 0;
		yValues[0] = avg;

		xValues[1] = throwsSize;
		yValues[1] = avg;

		series.setXSeries(xValues);
		series.setYSeries(yValues);
	}

	private void fillSeries(ISeries series, EList<Throw> throws1) {
		double[] xValues = new double[throws1.size() + 1];
		double[] yValues = new double[throws1.size() + 1];

		xValues[0] = 0;
		yValues[0] = 301;
		for (int i = 0; i < throws1.size(); i++) {
			double d = yValues[i] - throws1.get(i).getTotal();
			if (d >= 0) {
				yValues[i + 1] = d;
			} else {
				yValues[i + 1] = yValues[i];
			}
			xValues[i + 1] = i + 1;
		}

		series.setXSeries(xValues);
		series.setYSeries(yValues);
	}
}
