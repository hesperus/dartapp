package de.psi.pjf.dart.editor;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.swt.SWT;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import de.psi.pjf.dart.Game;

public class PlayerPanelHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		TreeSelection selection = (TreeSelection) HandlerUtil
				.getActiveMenuSelection(event);
		Game firstElement = (Game) selection.getFirstElement();

		try {
			PlatformUI
					.getWorkbench()
					.getActiveWorkbenchWindow()
					.getActivePage()
					.openEditor(new GameInput(firstElement),
							"de.psi.pjf.dart.editor.ActualThrowerEditor", false,IWorkbenchPage.MATCH_INPUT | IWorkbenchPage.MATCH_ID );
		} catch (PartInitException e) {
			e.printStackTrace();
		}
		
		return null;

	}

}
