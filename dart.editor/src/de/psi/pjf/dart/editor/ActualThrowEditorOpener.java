package de.psi.pjf.dart.editor;

import org.eclipse.emf.ecp.core.ECPProject;
import org.eclipse.emf.ecp.ui.util.ECPModelElementOpener;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import de.psi.pjf.dart.Game;

public class ActualThrowEditorOpener implements ECPModelElementOpener {

	@Override
	public void openModelElement(Object element, ECPProject ecpProject) {
		GameInput input = new GameInput((Game) element);
		try {
			PlatformUI
					.getWorkbench()
					.getActiveWorkbenchWindow()
					.getActivePage()
					.openEditor(input,
							"de.psi.pjf.dart.editor.ActualThrowerEditor", true);

		} catch (PartInitException e) {
		}
	}

}
