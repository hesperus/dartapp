package de.psi.pjf.dart.editor;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;

public class SimpleComposite<T extends EObject> extends Composite {

	static abstract class ValueProvider<T extends EObject> {
		abstract String getValue(T object);
	}

	private T object;
	private ValueProvider<T> aValueProvider;

	Label label;

	public SimpleComposite(Composite parent, T object,
			ValueProvider<T> aValueProvider) {
		super(parent, SWT.NONE);
		this.object = object;
		this.aValueProvider = aValueProvider;
		initializeLabel(parent);

		updateWidget();
	}

	private void initializeLabel(Composite parent) {
		label = new Label(parent, SWT.None);
		FontData fd = new FontData();
		fd.setHeight(20);
		fd.setStyle(SWT.BOLD);
		label.setFont(new Font(label.getDisplay(), fd));
	}

	void addContentListener() {
		this.object.eAdapters().add(new EContentAdapter() {
			@Override
			public void notifyChanged(Notification notification) {
				super.notifyChanged(notification);
				Display.getCurrent().asyncExec(new Runnable() {
					@Override
					public void run() {
						if (getParent() != null && !getParent().isDisposed())
							updateWidget();
					}
				});
			}
		});
	}

	private void updateWidget() {
		label.setText(aValueProvider.getValue(object));
	}

	void changeObject(T newObject) {
		object = newObject;
		updateWidget();
	}
}
