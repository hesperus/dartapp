//******************************************************************
//                                                                 
//  ThrowInput.java                                               
//  Copyright 2013 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.dart.editor;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;

import de.psi.pjf.dart.Game;

public class GameInput implements IEditorInput
{
    private final Game game_;

    public GameInput( Game element )
    {
        game_ = element;
    }

    @Override
    public Object getAdapter( @SuppressWarnings( "rawtypes" ) Class adapter )
    {
        return null;
    }

    @Override
    public boolean exists()
    {
        return true;
    }

    @Override
    public ImageDescriptor getImageDescriptor()
    {
        return null;
    }

    @Override
    public String getName()
    {
        return "Throw";
    }

    @Override
    public IPersistableElement getPersistable()
    {
        return null;
    }

    @Override
    public String getToolTipText()
    {
        return "Edit throw";
    }

    public Game getGame()
    {
        return game_;
    }

    @Override
    public boolean equals( Object arg0 )
    {
        if( arg0 instanceof GameInput )
        {
            return game_.equals( ((GameInput)arg0).getGame() );
        }
        else
        {
            return false;
        }
    }

}
