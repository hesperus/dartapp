package de.psi.pjf.dart.editor;

import java.util.Collection;
import java.util.Set;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.Collections2;
import com.google.common.collect.Sets;

import de.psi.pjf.dart.Game;
import de.psi.pjf.dart.Participation;

public class BestThrowComposite {

	private Game game;
	private Label bestThrow;
	private Label bestThrowPlayers;
	private Composite aParent;

	public BestThrowComposite(Game aGame) {
		this.game = aGame;

		aGame.eAdapters().add(extracted());
	}

	private EContentAdapter extracted() {
		return new EContentAdapter() {
			@Override
			public void notifyChanged(Notification notification) {
				super.notifyChanged(notification);
				Display.getCurrent().asyncExec(new Runnable() {
					@Override
					public void run() {
						if (aParent != null && !aParent.isDisposed())
							updateBestThrow();
					}
				});
			}
		};
	}

	public void init(Composite aParent) {
		this.aParent = aParent;
		Composite composite = new Composite(aParent, SWT.NONE);
		RowLayout layout = new RowLayout(SWT.HORIZONTAL);
		layout.justify=true;
		composite.setLayout(layout);

		bestThrow = new Label(composite, SWT.NONE);
		FontData fd = new FontData();
		fd.setHeight(20);
		fd.setStyle(SWT.BOLD);
		bestThrow.setFont(new Font(composite.getDisplay(), fd));

		bestThrowPlayers = new Label(composite, SWT.None);
		updateBestThrow();
	}

	private Set<Participation> findBestThrow() {
		Set<Participation> maxSet = null;
		int gameMax = -1;
		for (Participation part : game.getParticipations()) {
			if (part.getGameMax() > gameMax) {
				maxSet = Sets.newHashSet();
				maxSet.add(part);
				gameMax = part.getGameMax();
			} else if (part.getGameMax() == gameMax) {
				maxSet.add(part);
			}
		}
		return maxSet;
	}

	private void updateBestThrow() {
		Set<Participation> findBestThrow = findBestThrow();
		int gameMax = findBestThrow.iterator().next().getGameMax();
		Collection<String> transform = Collections2.transform(findBestThrow,
				new Function<Participation, String>() {
					@Override
					public String apply(Participation input) {
						return input.getPlayer().getName();
					}
				});
		bestThrow.setText(String.valueOf(gameMax));
		bestThrow.pack();

		bestThrowPlayers.setText(Joiner.on("\n").join(transform));
	}

	public void dispose() {

	}
}
