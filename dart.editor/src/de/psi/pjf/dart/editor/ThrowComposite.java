package de.psi.pjf.dart.editor;

import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.google.common.collect.Lists;

import de.psi.pjf.dart.Game;
import de.psi.pjf.dart.Participation;

public class ThrowComposite {

	private List<Participation> participations;
	private Game game;
	private Label label;
	private Composite aParent;

	public ThrowComposite(Game aGame) {
		this.game = aGame;
		participations = aGame.getParticipations();

		aGame.eAdapters().add(new EContentAdapter() {
			@Override
			public void notifyChanged(Notification notification) {
				super.notifyChanged(notification);
				if (aParent != null && !aParent.isDisposed())
					populate();
			}
		});
	}

	int player = 0;

	public void nextPlayer() {
		player = ++player % participations.size();
		populate();
	}

	public void init(Composite aParent) {
	
		this.aParent = aParent;
		label = new Label(aParent, SWT.NONE);
		FontData fd = new FontData();
		fd.setHeight(20);
		fd.setStyle(SWT.BOLD);
		label.setFont(new Font(aParent.getDisplay(), fd));
		populate();
	}

	public Participation getParticipation() {
		return participations.get(player);
	}

	private int findRound() {
		int round = 0;
		for (Participation participation : game.getParticipations()) {
			int size = participation.getThrows().size();
			round = size > round ? size : round;
		}
		return round;
	}

	private void populate() {
		label.setText(getParticipation().getPlayer().getName());
	}

}
