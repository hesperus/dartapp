package de.psi.pjf.dart.editor;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.part.EditorPart;

import com.google.common.collect.Lists;

import de.psi.pjf.dart.Game;
import de.psi.pjf.dart.Participation;

public class ActualThrowerEditor extends EditorPart {

	int actualParticipator = 0;
	private SimpleComposite<Participation> toGO;
	private Game game;
	private SimpleComposite<Participation> avg;
	private SimpleComposite<Participation> position;

	@Override
	public void doSave(IProgressMonitor monitor) {
		// TODO Auto-generated method stub

	}

	@Override
	public void doSaveAs() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {

		game = ((GameInput) input).getGame();
		setSite(site);
		setInput(input);
	}

	@Override
	public boolean isDirty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isSaveAsAllowed() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void createPartControl(Composite parent) {
		addKeyListener(parent);

		FormToolkit toolkit = new FormToolkit(parent.getDisplay());

		GridLayout layout = new GridLayout(3, true);
		parent.setLayout(layout);

		createAvgSection(parent, toolkit);
		createPositionSection(parent, toolkit);
		createToGoSection(parent, toolkit);

	}

	private void addKeyListener(Composite parent) {
		parent.getDisplay().addFilter(SWT.KeyDown, new Listener() {

			@Override
			public void handleEvent(Event event) {
				if (PlatformUI.getWorkbench().getActiveWorkbenchWindow()
						.getActivePage().getActiveEditor() == ActualThrowerEditor.this) {
					if (event.keyCode == SWT.SPACE) {
					}
				}
			}
		});
	}

	private void createToGoSection(Composite parent, FormToolkit toolkit) {
		Section section = toolkit.createSection(parent, Section.DESCRIPTION
				| Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		section.setText("To go:");
		GridData gridData = new GridData();
		gridData.verticalAlignment = SWT.FILL;
		gridData.horizontalAlignment = SWT.FILL;
		section.setLayoutData(gridData);

		Composite client = toolkit.createComposite(section, SWT.WRAP);

		client.setLayout(new FillLayout());

		toGO = new SimpleComposite<Participation>(client, game
				.getParticipations().get(actualParticipator),
				new SimpleComposite.ValueProvider<Participation>() {
					@Override
					String getValue(Participation object) {
						return String.valueOf(object.getToWin());
					}
				});

		section.setClient(client);
	}

	private void createAvgSection(Composite parent, FormToolkit toolkit) {
		Section section = toolkit.createSection(parent, Section.DESCRIPTION
				| Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		section.setText("Game avg:");
		GridData gridData = new GridData();
		gridData.verticalAlignment = SWT.FILL;
		gridData.horizontalAlignment = SWT.FILL;
		section.setLayoutData(gridData);

		Composite client = toolkit.createComposite(section, SWT.WRAP);

		client.setLayout(new FillLayout());

		avg = new SimpleComposite<Participation>(client, game
				.getParticipations().get(actualParticipator),
				new SimpleComposite.ValueProvider<Participation>() {
					@Override
					String getValue(Participation object) {
						return String.valueOf(object.getGameAvg());
					}
				});

		section.setClient(client);
	}

	private void createPositionSection(Composite parent, FormToolkit toolkit) {
		Section section = toolkit.createSection(parent, Section.DESCRIPTION
				| Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		section.setText("Game avg:");
		GridData gridData = new GridData();
		gridData.verticalAlignment = SWT.FILL;
		gridData.horizontalAlignment = SWT.FILL;
		section.setLayoutData(gridData);

		Composite client = toolkit.createComposite(section, SWT.WRAP);

		client.setLayout(new FillLayout());

		position = new SimpleComposite<Participation>(client, game
				.getParticipations().get(actualParticipator),
				new SimpleComposite.ValueProvider<Participation>() {
					@Override
					String getValue(Participation object) {
						List<Participation> participations = Lists
								.newArrayList(object.getGame()
										.getParticipations());
						Collections.sort(participations, Collections
								.reverseOrder(new Comparator<Participation>() {

									@Override
									public int compare(Participation o1,
											Participation o2) {
										return Integer.valueOf(o1.getToWin())
												.compareTo(o2.getToWin());
									}
								}));
						int i = 0;
						for (Participation participation : participations) {
							if (participation == object)
								break;
							else
								i++;
						}
						return String.valueOf(i);
					}
				});

		section.setClient(client);
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub

	}

}
