//******************************************************************
//                                                                 
//  ThrowInput.java                                               
//  Copyright 2013 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.dart.editor;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;

import de.psi.pjf.dart.Throw;

public class ThrowInput implements IEditorInput
{
    private final Throw throw_;

    public ThrowInput( Throw aThrow )
    {
        throw_ = aThrow;
    }

    @Override
    public Object getAdapter( @SuppressWarnings( "rawtypes" ) Class adapter )
    {
        return null;
    }

    @Override
    public boolean exists()
    {
        return true;
    }

    @Override
    public ImageDescriptor getImageDescriptor()
    {
        return null;
    }

    @Override
    public String getName()
    {
        return "Throw";
    }

    @Override
    public IPersistableElement getPersistable()
    {
        return null;
    }

    @Override
    public String getToolTipText()
    {
        return "Edit throw";
    }

    public Throw getThrow()
    {
        return throw_;
    }

    @Override
    public boolean equals( Object arg0 )
    {
        if( arg0 instanceof ThrowInput )
        {
            return throw_.equals( ((ThrowInput)arg0).getThrow() );
        }
        else
        {
            return false;
        }
    }

}
