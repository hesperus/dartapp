//******************************************************************
//                                                                 
//  ThrowEditor.java                                               
//  Copyright 2013 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.dart.editor;

import java.net.URL;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.EditorPart;

import de.psi.pjf.dart.DartPackage;
import de.psi.pjf.dart.Throw;
import de.psi.pjf.dart.ThrowType;
import de.psi.pjf.dart.presentation.DartEditorPlugin;

public class ThrowEditor extends EditorPart
{

    private Throw input;
    private int current;
    private static final EStructuralFeature[] POINTS =
    { DartPackage.Literals.THROW__FIRST, DartPackage.Literals.THROW__SECOND,
        DartPackage.Literals.THROW__THIRD };
    private static final EStructuralFeature[] TYPES =
    { DartPackage.Literals.THROW__FIRST_TYPE, DartPackage.Literals.THROW__SECOND_TYPE,
        DartPackage.Literals.THROW__THIRD_TYPE };
    private static final int[] SCORES =
    { 1, 18, 4, 13, 6, 10, 15, 2, 17, 3, 19, 7, 16, 8, 11, 14, 9, 12, 5, 20 };

    private boolean dirty;

    @Override
    public void doSave( IProgressMonitor monitor )
    {
    }

    @Override
    public void doSaveAs()
    {
    }

    @Override
    public void init( IEditorSite site, IEditorInput aInput ) throws PartInitException
    {
        if( !(aInput instanceof ThrowInput) )
        {
            throw new RuntimeException( "Wrong input" );
        }

        input = ((ThrowInput)aInput).getThrow();

        setSite( site );
        setInput( aInput );

        updateTitle();
    }

    private void updateTitle()
    {
        ComposedAdapterFactory composedAdapterFactory =
            new ComposedAdapterFactory( ComposedAdapterFactory.Descriptor.Registry.INSTANCE );

        AdapterFactoryLabelProvider labelProvider = new AdapterFactoryLabelProvider( composedAdapterFactory );

        setPartName( labelProvider.getText( input ) );
        setTitleImage( labelProvider.getImage( input ) );
    }

    @Override
    public boolean isDirty()
    {
        return dirty;
    }

    private class DartWidget extends Canvas
    {
        private final Image image;

        public DartWidget( Composite parent, Image aImage )
        {
            super( parent, 0 );
            image = aImage;
            final Color white = new Color( null, 255, 255, 255 );
            setBackground( white );

            addDisposeListener( new DisposeListener()
            {
                @Override
                public void widgetDisposed( DisposeEvent e )
                {
                    white.dispose();
                    image.dispose();
                }
            } );

            addPaintListener( new PaintListener()
            {

                @Override
                public void paintControl( PaintEvent e )
                {
                    GC gc = e.gc;
                    gc.drawImage( image, 0, 0, 893, 893, 0, 0, getBoardSize(), getBoardSize() );
                }
            } );

            addMouseListener( new MouseAdapter()
            {
                @Override
                public void mouseUp( org.eclipse.swt.events.MouseEvent e )
                {
                    if( e.x < getBoardSize() && e.y < getBoardSize() )
                    {
                        int half = getBoardSize() / 2;
                        updateScores( e.x - half, e.y - half, getBoardSize() );
                    }
                };
            } );
        }

        private int getBoardSize()
        {
            int x = DartWidget.this.getSize().x;
            int y = DartWidget.this.getSize().y;
            return Math.min( x, y );
        };
    }

    private void updateScores( int x, int y, double size )
    {
        int old = current;
        current = (current + 1) % 3;
        input.eSet( POINTS[ old ], 0 );
        input.eSet( TYPES[ old ], ThrowType.NORMAL );

        double d = (x * x + y * y) / (size * size);

        if( d > 0.137 )
        {
            return;
        }

        if( d < 0.0003225 )
        {
            input.eSet( POINTS[ old ], 25 );
            input.eSet( TYPES[ old ], ThrowType.DOUBLE );
            return;
        }

        if( d < 0.001365 )
        {
            input.eSet( POINTS[ old ], 25 );
            return;
        }

        if( d > 0.044 && d <= 0.0543 )
        {
            input.eSet( TYPES[ old ], ThrowType.TRIPLE );
        }

        if( d > 0.12 && d <= 0.137 )
        {
            input.eSet( TYPES[ old ], ThrowType.DOUBLE );
        }

        int index = (int)((Math.atan2( y, x ) * 180 / Math.PI + 442) % 360) / 18;
        input.eSet( POINTS[ old ], SCORES[ index ] );

        updateTitle();
    }

    @Override
    public boolean isSaveAsAllowed()
    {
        return false;
    }

    @Override
    public void createPartControl( Composite parent )
    {
        URL url = (URL)DartEditorPlugin.INSTANCE.getImage( "full/wizban/Dartboard.jpg" );
        new DartWidget( parent, ImageDescriptor.createFromURL( url ).createImage() );
    }

    @Override
    public void setFocus()
    {
    }

}
