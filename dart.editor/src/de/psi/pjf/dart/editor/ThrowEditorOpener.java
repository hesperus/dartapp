//******************************************************************
//                                                                 
//  ThrowEditorOpener.java                                               
//  Copyright 2013 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.dart.editor;

import org.eclipse.emf.ecp.core.ECPProject;
import org.eclipse.emf.ecp.ui.util.ECPModelElementOpener;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import de.psi.pjf.dart.Throw;

public class ThrowEditorOpener implements ECPModelElementOpener
{

    @Override
    public void openModelElement( Object element, ECPProject ecpProject )
    {
        ThrowInput input = new ThrowInput( (Throw)element );
        try
        {
            PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
                .openEditor( input, "de.psi.pjf.dart.editor.ThrowEditor", true );
        }
        catch( PartInitException e )
        {
        }
    }

}
