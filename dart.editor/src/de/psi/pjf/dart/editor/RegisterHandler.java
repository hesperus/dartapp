//******************************************************************
//                                                                 
//  RegisterHandler.java                                               
//  Copyright 2013 PSI AG. All rights reserved.              
//  PSI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms
//                                                                 
// ******************************************************************

package de.psi.pjf.dart.editor;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

public class RegisterHandler extends AbstractHandler
{

    @Override
    public Object execute( ExecutionEvent event ) throws ExecutionException
    {
        try
        {
            IViewPart part =
                PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
                    .showView( "org.eclipse.emf.ecp.ui.ModelExplorerView" );
            part.getClass();
            TreeSelection selection =
                (TreeSelection)part.getSite().getWorkbenchWindow().getSelectionService().getSelection();
            EPackage epackage = (EPackage)selection.getFirstElement();
            epackage.eClass();
            EPackage.Registry.INSTANCE.put( epackage.getNsURI(), epackage );
        }
        catch( PartInitException e )
        {
        }
        return null;
    }
}
