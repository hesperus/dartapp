package de.psi.pjf.dart.editor;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.part.EditorPart;

import de.psi.pjf.dart.Game;
import de.psi.pjf.dart.Participation;
import de.psi.pjf.dart.editor.SimpleComposite.ValueProvider;

public class GameEditor extends EditorPart {

	ChartComposite chart;
	private BestThrowComposite bestThrow;
	private ThrowComposite throwCmp;
	private Game game;

	@Override
	public void doSave(IProgressMonitor monitor) {
		// TODO Auto-generated method stub

	}

	@Override
	public void doSaveAs() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
		game = ((GameInput) input).getGame();
		chart = new ChartComposite(game);
		bestThrow = new BestThrowComposite(game);
		throwCmp = new ThrowComposite(game);
		// roundComposite = new RoundComposite(game);

		setSite(site);
		setInput(input);

	}

	@Override
	public boolean isDirty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isSaveAsAllowed() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void createPartControl(Composite parent) {

		parent.getDisplay().addFilter(SWT.KeyDown, new Listener() {

			@Override
			public void handleEvent(Event event) {
				if (PlatformUI.getWorkbench().getActiveWorkbenchWindow()
						.getActivePage().getActiveEditor() == GameEditor.this) {
					if (event.keyCode == SWT.SPACE) {
						throwCmp.nextPlayer();
					}
				}
			}
		});

		FormToolkit toolkit = new FormToolkit(parent.getDisplay());

		GridLayout layout = new GridLayout(3, true);
		parent.setLayout(layout);

		createBestThrowSection(parent, toolkit);

		createRoundSection(parent, toolkit);

		createActualPlayerSection(parent, toolkit);
		createChartSection(parent, toolkit);

	}

	private void createChartSection(Composite parent, FormToolkit toolkit) {
		Section section = toolkit.createSection(parent, Section.TWISTIE
				| Section.EXPANDED);

		Composite client = toolkit.createComposite(section, SWT.WRAP);

		GridData gridData = new GridData();
		gridData.horizontalSpan = 3;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		gridData.verticalAlignment = SWT.FILL;
		gridData.horizontalAlignment = SWT.FILL;
		section.setLayoutData(gridData);
		section.setLayout(new FillLayout());

		client.setLayout(new FillLayout());
		chart.init(client);
		section.setClient(client);
	}

	private void createRoundSection(Composite parent, FormToolkit toolkit) {
		Section section = toolkit.createSection(parent, Section.DESCRIPTION
				| Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		section.setText("Round nr:");

		GridData gridData = new GridData();
		gridData.verticalAlignment = SWT.FILL;
		gridData.horizontalAlignment = SWT.FILL;

		section.setLayoutData(gridData);

		Composite client = toolkit.createComposite(section, SWT.WRAP);

		client.setLayout(new FillLayout());

		SimpleComposite<Game> roundComposite = new SimpleComposite<Game>(
				client, game, new ValueProvider<Game>() {

					@Override
					String getValue(Game object) {
						int round = 1;
						for (Participation participation : game
								.getParticipations()) {
							int size = participation.getThrows().size();
							round = size > round ? size : round;
						}
						return String.valueOf(round);
					}

				});

		section.setClient(client);
	}

	private void createBestThrowSection(Composite parent, FormToolkit toolkit) {
		Section section = toolkit.createSection(parent, Section.DESCRIPTION
				| Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		section.setText("Best game's throw:");
		GridData gridData = new GridData();
		gridData.verticalAlignment = SWT.FILL;
		gridData.horizontalAlignment = SWT.FILL;
		section.setLayoutData(gridData);

		Composite client = toolkit.createComposite(section, SWT.WRAP);

		client.setLayout(new FillLayout());

		bestThrow.init(client);
		section.setClient(client);
	}

	private void createActualPlayerSection(Composite parent, FormToolkit toolkit) {
		Section section = toolkit.createSection(parent, Section.DESCRIPTION
				| Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		section.setText("Actual player:");
		GridData gridData = new GridData();
		gridData.verticalAlignment = SWT.FILL;
		gridData.horizontalAlignment = SWT.FILL;
		section.setLayoutData(gridData);
		Composite client = toolkit.createComposite(section, SWT.WRAP);

		client.setLayout(new FillLayout());

		throwCmp.init(client);

		section.setClient(client);
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub

	}

}
