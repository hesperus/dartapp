/**
 */
package de.psi.pjf.dart.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import de.psi.pjf.dart.DartFactory;
import de.psi.pjf.dart.DartPackage;
import de.psi.pjf.dart.Participation;
import de.psi.pjf.dart.Player;

/**
 * This is the item provider adapter for a {@link de.psi.pjf.dart.Participation} object.
 * <!-- begin-user-doc
 * --> <!-- end-user-doc -->
 * @generated
 */
public class ParticipationItemProvider extends ItemProviderAdapter implements IEditingDomainItemProvider,
    IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource
{
    /**
     * This constructs an instance from a factory and a notifier. <!-- begin-user-doc --> <!-- end-user-doc
     * -->
     * 
     * @generated
     */
    public ParticipationItemProvider( AdapterFactory adapterFactory )
    {
    super(adapterFactory);
  }

    /**
     * This returns the property descriptors for the adapted class. <!-- begin-user-doc --> <!-- end-user-doc
     * -->
     * 
     * @generated NOT
     */
    @Override
    public List< IItemPropertyDescriptor > getPropertyDescriptors( Object object )
    {
        if( itemPropertyDescriptors == null )
        {
            super.getPropertyDescriptors( object );

            addPlayerPropertyDescriptor( object );
            addToWinPropertyDescriptor( object );
            addGameMaxPropertyDescriptor( object );
            addWinnerPropertyDescriptor( object );
        }
        return itemPropertyDescriptors;
    }

    /**
   * This adds a property descriptor for the Game feature.
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    protected void addGamePropertyDescriptor( Object object )
    {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_Participation_game_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_Participation_game_feature", "_UI_Participation_type"),
         DartPackage.Literals.PARTICIPATION__GAME,
         true,
         false,
         true,
         null,
         null,
         null));
  }

    /**
   * This adds a property descriptor for the Player feature.
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    protected void addPlayerPropertyDescriptor( Object object )
    {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_Participation_player_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_Participation_player_feature", "_UI_Participation_type"),
         DartPackage.Literals.PARTICIPATION__PLAYER,
         true,
         false,
         true,
         null,
         null,
         null));
  }

    /**
   * This adds a property descriptor for the To Win feature.
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    protected void addToWinPropertyDescriptor( Object object )
    {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_Participation_toWin_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_Participation_toWin_feature", "_UI_Participation_type"),
         DartPackage.Literals.PARTICIPATION__TO_WIN,
         false,
         false,
         false,
         ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
         null,
         null));
  }

    /**
   * This adds a property descriptor for the Game Max feature.
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    protected void addGameMaxPropertyDescriptor( Object object )
    {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_Participation_gameMax_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_Participation_gameMax_feature", "_UI_Participation_type"),
         DartPackage.Literals.PARTICIPATION__GAME_MAX,
         false,
         false,
         false,
         ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
         null,
         null));
  }

    /**
   * This adds a property descriptor for the Winner feature.
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    protected void addWinnerPropertyDescriptor( Object object )
    {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_Participation_winner_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_Participation_winner_feature", "_UI_Participation_type"),
         DartPackage.Literals.PARTICIPATION__WINNER,
         false,
         false,
         false,
         ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
         null,
         null));
  }

    /**
   * This adds a property descriptor for the Game Avg feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addGameAvgPropertyDescriptor(Object object)
  {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_Participation_gameAvg_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_Participation_gameAvg_feature", "_UI_Participation_type"),
         DartPackage.Literals.PARTICIPATION__GAME_AVG,
         false,
         false,
         false,
         ItemPropertyDescriptor.REAL_VALUE_IMAGE,
         null,
         null));
  }

    /**
   * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
   * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
   * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
   * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
   * @generated
   */
    @Override
    public Collection<? extends EStructuralFeature> getChildrenFeatures( Object object )
    {
    if (childrenFeatures == null)
    {
      super.getChildrenFeatures(object);
      childrenFeatures.add(DartPackage.Literals.PARTICIPATION__THROWS);
    }
    return childrenFeatures;
  }

    /**
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    @Override
    protected EStructuralFeature getChildFeature( Object object, Object child )
    {
    // Check the type of the specified child object and return the proper feature to use for
    // adding (see {@link AddCommand}) it as a child.

    return super.getChildFeature(object, child);
  }

    /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected boolean shouldComposeCreationImage() 
  {
    return true;
  }

    /**
     * This returns Participation.gif. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public Object getImage( Object object )
    {
        String winner = ((Participation)object).isWinner() ? "_winner" : "";

        return overlayImage( object, getResourceLocator().getImage( "full/obj16/Player" + winner + ".png" ) );
    }

    /**
     * This returns the label text for the adapted class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public String getText( Object object )
    {
        Participation participation = (Participation)object;
        Player player = participation.getPlayer();

        StringBuilder label = new StringBuilder( player == null ? "Unknown" : player.getName() );
        label.append( "(to win: " );
        label.append( participation.getToWin() );
        label.append( ", game max: " );
        label.append( participation.getGameMax() );
        label.append( " )" );

        return label.toString();
    }

    /**
   * This handles model notifications by calling {@link #updateChildren} to update any cached
   * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
   * <!-- begin-user-doc
     * --> <!-- end-user-doc -->
   * @generated
   */
    @Override
    public void notifyChanged( Notification notification )
    {
    updateChildren(notification);

    switch (notification.getFeatureID(Participation.class))
    {
      case DartPackage.PARTICIPATION__TO_WIN:
      case DartPackage.PARTICIPATION__GAME_MAX:
      case DartPackage.PARTICIPATION__WINNER:
      case DartPackage.PARTICIPATION__GAME_AVG:
        fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
        return;
      case DartPackage.PARTICIPATION__THROWS:
        fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
        return;
    }
    super.notifyChanged(notification);
  }

    /**
   * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
   * that can be created under this object.
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    @Override
    protected void collectNewChildDescriptors( Collection<Object> newChildDescriptors, Object object )
    {
    super.collectNewChildDescriptors(newChildDescriptors, object);

    newChildDescriptors.add
      (createChildParameter
        (DartPackage.Literals.PARTICIPATION__THROWS,
         DartFactory.eINSTANCE.createThrow()));
  }

    /**
   * Return the resource locator for this item provider's resources.
   * <!-- begin-user-doc --> <!--
     * end-user-doc -->
   * @generated
   */
    @Override
    public ResourceLocator getResourceLocator()
    {
    return DartEditPlugin.INSTANCE;
  }

}
