/**
 */
package de.psi.pjf.dart.provider;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.List;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;
import de.psi.pjf.dart.DartFactory;
import de.psi.pjf.dart.DartPackage;
import de.psi.pjf.dart.Game;
import de.psi.pjf.dart.Player;

/**
 * This is the item provider adapter for a {@link de.psi.pjf.dart.Game} object.
 * <!-- begin-user-doc --> <!--
 * end-user-doc -->
 * @generated
 */
public class GameItemProvider extends ItemProviderAdapter implements IEditingDomainItemProvider,
    IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource
{
    /**
     * This constructs an instance from a factory and a notifier. <!-- begin-user-doc --> <!-- end-user-doc
     * -->
     * 
     * @generated
     */
    public GameItemProvider( AdapterFactory adapterFactory )
    {
    super(adapterFactory);
  }

    /**
     * This returns the property descriptors for the adapted class. <!-- begin-user-doc --> <!-- end-user-doc
     * -->
     * 
     * @generated NOT
     */
    @Override
    public List< IItemPropertyDescriptor > getPropertyDescriptors( Object object )
    {
        if( itemPropertyDescriptors == null )
        {
            super.getPropertyDescriptors( object );

            addGameTypePropertyDescriptor( object );
        }
        return itemPropertyDescriptors;
    }

    /**
   * This adds a property descriptor for the League feature.
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    protected void addLeaguePropertyDescriptor( Object object )
    {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_Game_league_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_Game_league_feature", "_UI_Game_type"),
         DartPackage.Literals.GAME__LEAGUE,
         true,
         false,
         true,
         null,
         null,
         null));
  }

    /**
     * This adds a property descriptor for the Game Type feature. <!-- begin-user-doc --> <!-- end-user-doc
     * -->
     * 
     * @generated
     */
    protected void addGameTypePropertyDescriptor( Object object )
    {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_Game_gameType_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_Game_gameType_feature", "_UI_Game_type"),
         DartPackage.Literals.GAME__GAME_TYPE,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

    /**
   * This adds a property descriptor for the Winners feature.
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    protected void addWinnersPropertyDescriptor( Object object )
    {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_Game_winners_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_Game_winners_feature", "_UI_Game_type"),
         DartPackage.Literals.GAME__WINNERS,
         false,
         false,
         false,
         null,
         null,
         null));
  }

    /**
   * This adds a property descriptor for the Created feature.
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    protected void addCreatedPropertyDescriptor( Object object )
    {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_Game_created_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_Game_created_feature", "_UI_Game_type"),
         DartPackage.Literals.GAME__CREATED,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

    /**
   * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
   * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
   * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
   * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
   * @generated
   */
    @Override
    public Collection<? extends EStructuralFeature> getChildrenFeatures( Object object )
    {
    if (childrenFeatures == null)
    {
      super.getChildrenFeatures(object);
      childrenFeatures.add(DartPackage.Literals.GAME__PARTICIPATIONS);
    }
    return childrenFeatures;
  }

    /**
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    @Override
    protected EStructuralFeature getChildFeature( Object object, Object child )
    {
    // Check the type of the specified child object and return the proper feature to use for
    // adding (see {@link AddCommand}) it as a child.

    return super.getChildFeature(object, child);
  }

    /**
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    @Override
    protected boolean shouldComposeCreationImage()
    {
    return true;
  }

    /**
     * This returns Game.gif. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public Object getImage( Object object )
    {
        return overlayImage( object, getResourceLocator().getImage( "full/obj16/Game.png" ) );
    }

    /**
     * This returns the label text for the adapted class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public String getText( Object object )
    {

        Game game = (Game)object;
        StringBuilder label = new StringBuilder( "Game " );
        label.append( game.getGameType().getValue() ).append( " " );
        if( game.getCreated() != null )
        {
            label.append( new SimpleDateFormat( "dd-MM-yyyy HH:mm" ).format( game.getCreated() ) );
        }

        EList< Player > winners = game.getWinners();
        if( !winners.isEmpty() )
        {
            label.append( "(winners: " );
            boolean first = true;
            for( Player p : winners )
            {
                if( !first )
                {
                    label.append( ", " );
                }

                label.append( p.getName() );

                first = false;
            }
            label.append( " )" );
        }

        return label.toString();
    }

    /**
   * This handles model notifications by calling {@link #updateChildren} to update any cached
   * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
   * <!-- begin-user-doc
     * --> <!-- end-user-doc -->
   * @generated
   */
    @Override
    public void notifyChanged( Notification notification )
    {
    updateChildren(notification);

    switch (notification.getFeatureID(Game.class))
    {
      case DartPackage.GAME__GAME_TYPE:
      case DartPackage.GAME__CREATED:
        fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
        return;
      case DartPackage.GAME__PARTICIPATIONS:
        fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
        return;
    }
    super.notifyChanged(notification);
  }

    /**
   * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
   * that can be created under this object.
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    @Override
    protected void collectNewChildDescriptors( Collection<Object> newChildDescriptors, Object object )
    {
    super.collectNewChildDescriptors(newChildDescriptors, object);

    newChildDescriptors.add
      (createChildParameter
        (DartPackage.Literals.GAME__PARTICIPATIONS,
         DartFactory.eINSTANCE.createParticipation()));
  }

    /**
   * Return the resource locator for this item provider's resources.
   * <!-- begin-user-doc --> <!--
     * end-user-doc -->
   * @generated
   */
    @Override
    public ResourceLocator getResourceLocator()
    {
    return DartEditPlugin.INSTANCE;
  }

}
