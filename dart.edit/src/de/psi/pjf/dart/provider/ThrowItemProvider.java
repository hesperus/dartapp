/**
 */
package de.psi.pjf.dart.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import de.psi.pjf.dart.DartPackage;
import de.psi.pjf.dart.Throw;

/**
 * This is the item provider adapter for a {@link de.psi.pjf.dart.Throw} object.
 * <!-- begin-user-doc --> <!--
 * end-user-doc -->
 * @generated
 */
public class ThrowItemProvider extends ItemProviderAdapter implements IEditingDomainItemProvider,
    IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource
{
    /**
     * This constructs an instance from a factory and a notifier. <!-- begin-user-doc --> <!-- end-user-doc
     * -->
     * 
     * @generated
     */
    public ThrowItemProvider( AdapterFactory adapterFactory )
    {
    super(adapterFactory);
  }

    /**
     * This returns the property descriptors for the adapted class. <!-- begin-user-doc --> <!-- end-user-doc
     * -->
     * 
     * @generated
     */
    @Override
    public List<IItemPropertyDescriptor> getPropertyDescriptors( Object object )
    {
    if (itemPropertyDescriptors == null)
    {
      super.getPropertyDescriptors(object);

      addParticipationPropertyDescriptor(object);
      addFirstPropertyDescriptor(object);
      addFirstTypePropertyDescriptor(object);
      addSecondPropertyDescriptor(object);
      addSecondTypePropertyDescriptor(object);
      addThirdPropertyDescriptor(object);
      addThirdTypePropertyDescriptor(object);
      addTotalPropertyDescriptor(object);
      addDoublesPropertyDescriptor(object);
      addTriplesPropertyDescriptor(object);
      addBestPropertyDescriptor(object);
    }
    return itemPropertyDescriptors;
  }

    /**
   * This adds a property descriptor for the Participation feature.
   * <!-- begin-user-doc --> <!--
     * end-user-doc -->
   * @generated
   */
    protected void addParticipationPropertyDescriptor( Object object )
    {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_Throw_participation_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_Throw_participation_feature", "_UI_Throw_type"),
         DartPackage.Literals.THROW__PARTICIPATION,
         true,
         false,
         true,
         null,
         null,
         null));
  }

    /**
   * This adds a property descriptor for the First feature.
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    protected void addFirstPropertyDescriptor( Object object )
    {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_Throw_first_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_Throw_first_feature", "_UI_Throw_type"),
         DartPackage.Literals.THROW__FIRST,
         true,
         false,
         false,
         ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
         null,
         null));
  }

    /**
     * This adds a property descriptor for the First Type feature. <!-- begin-user-doc --> <!-- end-user-doc
     * -->
     * 
     * @generated
     */
    protected void addFirstTypePropertyDescriptor( Object object )
    {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_Throw_firstType_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_Throw_firstType_feature", "_UI_Throw_type"),
         DartPackage.Literals.THROW__FIRST_TYPE,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

    /**
   * This adds a property descriptor for the Second feature.
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    protected void addSecondPropertyDescriptor( Object object )
    {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_Throw_second_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_Throw_second_feature", "_UI_Throw_type"),
         DartPackage.Literals.THROW__SECOND,
         true,
         false,
         false,
         ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
         null,
         null));
  }

    /**
     * This adds a property descriptor for the Second Type feature. <!-- begin-user-doc --> <!-- end-user-doc
     * -->
     * 
     * @generated
     */
    protected void addSecondTypePropertyDescriptor( Object object )
    {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_Throw_secondType_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_Throw_secondType_feature", "_UI_Throw_type"),
         DartPackage.Literals.THROW__SECOND_TYPE,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

    /**
   * This adds a property descriptor for the Third feature.
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    protected void addThirdPropertyDescriptor( Object object )
    {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_Throw_third_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_Throw_third_feature", "_UI_Throw_type"),
         DartPackage.Literals.THROW__THIRD,
         true,
         false,
         false,
         ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
         null,
         null));
  }

    /**
     * This adds a property descriptor for the Third Type feature. <!-- begin-user-doc --> <!-- end-user-doc
     * -->
     * 
     * @generated
     */
    protected void addThirdTypePropertyDescriptor( Object object )
    {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_Throw_thirdType_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_Throw_thirdType_feature", "_UI_Throw_type"),
         DartPackage.Literals.THROW__THIRD_TYPE,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

    /**
   * This adds a property descriptor for the Total feature.
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    protected void addTotalPropertyDescriptor( Object object )
    {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_Throw_total_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_Throw_total_feature", "_UI_Throw_type"),
         DartPackage.Literals.THROW__TOTAL,
         false,
         false,
         false,
         ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
         null,
         null));
  }

    /**
   * This adds a property descriptor for the Doubles feature.
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    protected void addDoublesPropertyDescriptor( Object object )
    {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_Throw_doubles_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_Throw_doubles_feature", "_UI_Throw_type"),
         DartPackage.Literals.THROW__DOUBLES,
         false,
         false,
         false,
         ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
         null,
         null));
  }

    /**
   * This adds a property descriptor for the Triples feature.
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    protected void addTriplesPropertyDescriptor( Object object )
    {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_Throw_triples_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_Throw_triples_feature", "_UI_Throw_type"),
         DartPackage.Literals.THROW__TRIPLES,
         false,
         false,
         false,
         ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
         null,
         null));
  }

    /**
   * This adds a property descriptor for the Best feature.
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    protected void addBestPropertyDescriptor( Object object )
    {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_Throw_best_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_Throw_best_feature", "_UI_Throw_type"),
         DartPackage.Literals.THROW__BEST,
         false,
         false,
         false,
         ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
         null,
         null));
  }

    /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected boolean shouldComposeCreationImage() 
  {
    return true;
  }

    /**
     * This returns Throw.gif. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public Object getImage( Object object )
    {
        Throw tr = (Throw)object;
        String label = tr.isBest() ? "_winner" : "";
        return overlayImage( object, getResourceLocator().getImage( "full/obj16/Throw" + label + ".png" ) );
    }

    /**
     * This returns the label text for the adapted class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public String getText( Object object )
    {
        Throw throw_ = (Throw)object;
        StringBuilder label = new StringBuilder( "Total: " );
        label.append( throw_.getTotal() ).append( ", " );
        label.append( throw_.getFirstType().getValue() ).append( "x" ).append( throw_.getFirst() )
            .append( ", " );
        label.append( throw_.getSecondType().getValue() ).append( "x" ).append( throw_.getSecond() )
            .append( ", " );
        label.append( throw_.getThirdType().getValue() ).append( "x" ).append( throw_.getThird() );
        return label.toString();
    }

    /**
   * This handles model notifications by calling {@link #updateChildren} to update any cached
   * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
   * <!-- begin-user-doc
     * --> <!-- end-user-doc -->
   * @generated
   */
    @Override
    public void notifyChanged( Notification notification )
    {
    updateChildren(notification);

    switch (notification.getFeatureID(Throw.class))
    {
      case DartPackage.THROW__FIRST:
      case DartPackage.THROW__FIRST_TYPE:
      case DartPackage.THROW__SECOND:
      case DartPackage.THROW__SECOND_TYPE:
      case DartPackage.THROW__THIRD:
      case DartPackage.THROW__THIRD_TYPE:
      case DartPackage.THROW__TOTAL:
      case DartPackage.THROW__DOUBLES:
      case DartPackage.THROW__TRIPLES:
      case DartPackage.THROW__BEST:
        fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
        return;
    }
    super.notifyChanged(notification);
  }

    /**
   * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
   * that can be created under this object.
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
    @Override
    protected void collectNewChildDescriptors( Collection<Object> newChildDescriptors, Object object )
    {
    super.collectNewChildDescriptors(newChildDescriptors, object);
  }

    /**
   * Return the resource locator for this item provider's resources.
   * <!-- begin-user-doc --> <!--
     * end-user-doc -->
   * @generated
   */
    @Override
    public ResourceLocator getResourceLocator()
    {
    return DartEditPlugin.INSTANCE;
  }

}
